Lectures on Basic Mathematics
-----------------------------

Copyright 2018 Lasse Kliemann <lasse@lassekliemann.de>

PDF File
--------

A PDF file of the latest master version should be
[here](https://gitlab.com/lxkl/lec-basic-math/-/jobs/artifacts/master/raw/BasicMath.pdf?job=compile_pdf).

Check below under "Corrections" to get an idea about the maturity of the different chapters.

Compiling
---------

The main file is `BasicMath.tex` and should be compiled with `pdflatex`.
Prerequisites are:

* A current and complete installtion of [TeX Live](https://www.tug.org/texlive/).
Under Debian, the following should be enough:
```
sudo apt-get install texlive '^texlive-.*'
```
* A directory with the files from
https://gitlab.com/lxkl/latex.git
must be listed in the `TEXINPUTS` environment variable.
When setting `TEXINPUTS`, make sure that the empty path is included,
otherwise TeX will not find its own files.

Corrections
-----------

Please push your corrections to your own corrections branch.
It is named `IDENT-corrections`,
where `IDENT` is your personal handle.
Please contact me to obtain your handle and corrections branch.

License
-------

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
