\chapter{Systems of Linear Equations}
\label{cha:cha230}

\section{Fundamentals}

\begin{definition}
  Let $\field$ be a field and $d,n \in \NN$.
  Let $A \in \field^{d \times n}$ and $b \in \field^d$.
  We call the pair $(A,b)$ a \term{system of linear equations} (SLE)
  with $d$ \term{equations} and $n$ \term{variables} or \term{unknowns}
  over the field $\field$.
  The matrix $A$ is called the \term{coefficient matrix} of the SLE
  and its entries are called the \term{coefficients} of the SLE.
  The vector $b$ is called the \term{right\-/hand side} (RHS) of the SLE.
  The set
  \begin{IEEEeqnarray*}{c}
    \cL(A,b) \df \set{x \in \field^n \suchthat Ax=b}
  \end{IEEEeqnarray*}
  is called the set of \term{solutions} or the \term{solution space} of $(A,b)$.
  The elements of $\cL(A,b)$ are called \term{solutions} of $(A,b)$.
  If $\cL(A,b) = \emptyset$, then $(A,b)$ is called \term{non\-/solvable},
  and otherwise it is called \term{solvable}.
  \qed
\end{definition}

In applications, SLE often arise in a different form.
For example, we may ask for all $x_1, x_2, x_3, x_4 \in \RR$ such that:
\begin{IEEEeqnarray*}{l}
  x_1 = -2 x_3 + 7 \\
  4x_2 + x_3 - x_4 + 1 = x_1 \\
  x_1 + x_2 = x_3 - x_4 - 5
\end{IEEEeqnarray*}
Written a bit differently, we can already see the similarity to an SLE:
\begin{IEEEeqnarray*}{l}
  x_1 + 0x_2 + 2 x_3 + 0x_4 = 7 \\
  -x_1 + 4x_2 + x_3 - x_4 = -1 \\
  x_1 + x_2 - x_3 + x_4 = - 5
\end{IEEEeqnarray*}
Hence, we are in fact looking for the solutions of the SLE with coefficient matrix $A \in \RR^{3 \times 4}$
and RHS $b \in \RR^3$, where:
\begin{IEEEeqnarray*}{c+c}
  A =
  \begin{bmatrix}
    1 & 0 & 2 & 0 \\
    -1 & 4 & 1 & -1 \\
    1 & 1 & -1 & 1
  \end{bmatrix}
  &
  b = \colvec{7 \\ -1 \\ -5}
\end{IEEEeqnarray*}

\begin{theorem}
  Let $(A,b)$ be a solvable SLE and $u \in \cL(A,b)$ any solution.
  Then $\cL(A,b) = u + \ker(A)$. 
\end{theorem}
\begin{proof}
  $\subseteq$)
  Let $x \in \cL(A,b)$.
  Then $A (x-u) = Ax - Au = b - b = 0$, hence $x-u \in \ker(A)$.
  Hence $x = u + (x-u) \in u + \ker(A)$.
  \par
  $\supseteq$)
  Let $x \in u+\ker(A)$.
  Then we find $y \in \ker(A)$ such that $x = u + y$.
  It follows $Ax = A(u+y) = Au + Ay = b + 0 = b$,
  hence $x \in \cL(A,b)$.
\end{proof}

In order to completely describe $\cL(A,b)$ for a solvable SLE $(A,b)$, we can thus give the following:
\begin{itemize}
\item A solution $u$ of $(A,b)$, that is, any element $u \in \cL(A,b)$.
\item A basis $K = \seti{v_i}{k} \subseteq \field^n$ of $A$'s kernel $\ker(A)$, where $k = \dim(\ker(A))$.
\end{itemize}
Then we have $\cL(A,b) = u + \spann(K)$,
that is, we obtain all the solutions by plugging all possible $\lam \in \field^k$ into:
\begin{equation*}
  u + \sum_{i=1}^k \lam_i v_i
\end{equation*}
In order to obtain $u$ and $K$ for a concrete SLE, we use \term{elementary row operations},
which will be introduced in the next section.

\begin{remark}
  \label{rem:cha230:rank-equal}
  An SLE $(A,b)$ is solvable if and only if $\rank(A) = \rank(\extcoeff{A}{b})$.
\end{remark}
\begin{proof}
  Denote $\cA \df \set{A_{\ast,1}, \hdots, A_{\ast,n}}$.
  We have:
  \begin{IEEEeqnarray*}{rCl"s+x*}
    && \text{$(A,b)$ solvable} \\
    &\iff&
    b \in \spann(\cA) \\
    &\iff&
    \spann(\cA) = \spann(\cA \cup \set{b})
    & by \autoref{prop:cha210:span-subspace} \\
    &\iff&
    \dim(\spann(\cA)) = \dim(\spann(\cA \cup \set{b}))
    & by \autoref{thm:cha210:sub-dim} \\
    &\iff&
    \rank(A) = \rank(\extcoeff{A}{b})
    && \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\section{Elementary Row Operations}

\begin{definition}
  \label{def:cha230:elem-row-op}
  The following operations transform a matrix into a matrix with the same number of rows and columns.
  They are called \term{elementary row operations}:
  \begin{itemize}
  \item Swapping of two rows.
  \item Multiplying a row with a non\-/zero scalar, also called \term{scaling} this row by the particular scalar.
  \item Adding a scalar multiple of one row to another row.\qed
  \end{itemize}
\end{definition}

We demonstrate each type of elementary row operation by an example:
\begin{IEEEeqnarray*}{l+C+l}
  \begin{gmatrix}[b]
    1 & 2 & 3 & 4 \\
    5 & 6 & 7 & 8 \\
    9 & 10 & 11 & 12
    \rowops
    \swap{0}{1}
  \end{gmatrix}
  &\longrightarrow&
  \begin{gmatrix}[b]
    5 & 6 & 7 & 8 \\
    1 & 2 & 3 & 4 \\
    9 & 10 & 11 & 12
  \end{gmatrix}
  \\
  \begin{gmatrix}[b]
    1 & 2 & 3 & 4 \\
    5 & 6 & 7 & 8 \\
    9 & 10 & 11 & 12
    \rowops
    \mult{0}{\cdot 5}
  \end{gmatrix}
  &\longrightarrow&
  \begin{gmatrix}[b]
    5 & 10 & 15 & 20 \\
    5 & 6 & 7 & 8 \\
    9 & 10 & 11 & 12
  \end{gmatrix}
  \\
  \begin{gmatrix}[b]
    1 & 2 & 3 & 4 \\
    5 & 6 & 7 & 8 \\
    9 & 10 & 11 & 12
    \rowops
    \add[2]{1}{2}
  \end{gmatrix}
  &\longrightarrow&
  \begin{gmatrix}[b]
    1 & 2 & 3 & 4 \\
    5 & 6 & 7 & 8 \\
    19 & 22 & 25 & 28
  \end{gmatrix}
\end{IEEEeqnarray*}
\par\smallskip

\begin{definition}
  Let $(A,b)$ be an SLE with $d$ equations and $n$ variables over a field~$\field$.
  When we say that $(A',b')$ is obtained from $(A,b)$ by certain elementary row operations,
  we mean that $A'$ is obtained from $A$ by those elementary row operations,
  and $b'$ is obtained from $b$ by the \emphasis{same} elementary row operations.
  \qed
\end{definition}

\begin{proposition}
  \label{prop:cha230:row-op}
  Let $(A,b)$ be an SLE with $d$ equations and $n$ variables over a field~$\field$.
  Assume that $(A',b')$ is obtained from $(A,b)$ by a series of elementary row operations.
  Then we have:
  \begin{enumerate}
  \item\label{prop:cha230:row-op:1} $\cL(A,b) = \cL(A',b')$
  \item\label{prop:cha230:row-op:2} $\ker(A) = \ker(A')$
  \end{enumerate}
\end{proposition}
\begin{proof}
  \ref*{prop:cha230:row-op:2} follows from~\ref*{prop:cha230:row-op:1}
  by considering the special case $b=0$.
  Hence we only have to prove~\ref*{prop:cha230:row-op:1}.
  \par
  It suffices to show that each elementary row operation maintains the set of solutions.
  The statement is clear for row swapping and multiplying with a non\-/zero scalar.
  Hence consider that $(A',b')$ was obtained from $(A,b)$
  by adding $\al$ times the $r$th row to the $t$th row of $A$ and of $b$
  for some $r,t \in \setn{d}$, $r \neq t$, and some $\al \in \fieldnz$.
  Let $x \in \field^n$. We prove $Ax=b \iff A'x=b'$.
  \par
  $\implies$)
  It suffices to show $\sum_{i=1}^n A'_{t,i} x_i = b'_t$.
  We have:
  \begin{IEEEeqnarray*}{c}
    \sum_{i=1}^n A'_{t,i} x_i
    = \sum_{i=1}^n (\al A_{r,i} + A_{t,i}) x_i
    = \al \sum_{i=1}^n A_{r,i} x_i + \sum_{i=1}^n A_{t,i} x_i
    = \al b_r + b_t
    = b'_t
  \end{IEEEeqnarray*}
  $\impliedby$)
  It suffices to show $\sum_{i=1}^n A_{t,i} x_i = b_t$.
  We have:
  \begin{IEEEeqnarray*}{rCl+x*}
    \sum_{i=1}^n A_{t,i} x_i
    &=& \sum_{i=1}^n (\al A_{r,i} + A_{t,i}) x_i - \al \sum_{i=1}^n A_{r,i} x_i \\
    &=& \sum_{i=1}^n A'_{t,i} x_i - \al \sum_{i=1}^n A'_{r,i} x_i \\
    &=& b'_t - \al b'_r = \al b_r + b_t - \al b_r = b_t
    & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{remark}
  \label{rem:cha230:col-swap}
  Let $\field$ be a field and $A \in \field^{d \times n}$ for some $d,n \in \NN$,
  and let $x \in \field^n$.
  Let $r,t \in \setn{n}$ and let $\tiA$ result from $A$ by swapping column $r$ with column $t$,
  and let $\tix$ result from $x$ by swapping entry $r$ with entry $t$
  (that is, $\tix_r = x_t$, $\tix_t = x_r$, and $\tix_i = x_i$ for all $i \not \in \set{r,t}$).
  Then $Ax = \tiA\tix$.
\end{remark}
\begin{proof}
  Follows directly from \autoref{prop:cha220:Ax-lin}~\ref{prop:cha220:Ax-lin:1}.
\end{proof}

\begin{proposition}
  \label{prop:cha230:reverse-col-swaps}
  Let $(A,b)$ be an SLE with $d$ equations and $n$ variables over a field~$\field$.
  Assume that $(A',b')$ is obtained from $(A,b)$ by a series of elementary row operations
  and column swaps, where the latter are only performed on $A$.
  Denote by ${\seti{(r_i,t_i)}{N} \subseteq \setn{n}^2}$ the column swaps in the order in which they were performed.
  For each $x \in \field^n$ denote by $\tix$ the vector that results from $x$ by swapping entries
  in the order $(r_N,t_N), (r_{N-1},t_{N-1}), \hdots, (r_1,t_1)$,
  that is, first we swap entry $r_N$ with entry $t_N$,
  then entry $r_{N-1}$ with entry $t_{N-1}$, and so on.
  Then for each $x \in \field^n$ we have:
  \begin{enumerate}
  \item\label{prop:cha230:reverse-col-swaps:1} $x \in \cL(A',b') \iff \tix \in \cL(A,b)$
  \item\label{prop:cha230:reverse-col-swaps:2} $x \in \ker(A') \iff \tix \in \ker(A)$
  \end{enumerate}
\end{proposition}
\begin{proof}
  \ref*{prop:cha230:reverse-col-swaps:2} follows from~\ref*{prop:cha230:reverse-col-swaps:1}
  by considering the special case $b=0$.
  Hence we only have to prove~\ref*{prop:cha230:reverse-col-swaps:1}.
  \par
  We can obtain $(A',b')$ from $(A,b)$
  by first performing all the elementary row operations and then all the column swaps.
  By \autoref{prop:cha230:row-op}, we may ignore the elementary row operations
  and only consider the column swaps.
  So we may assume that $A'$ results from $A$
  by the column swaps $(r_1,t_1), (r_{2},t_{2}), \hdots, (r_N,t_N)$, and that $b=b'$.
  \par
  $\implies$)
  We transform $A'$ into $A$ by conducting the swaps in reverse order.
  By \autoref{rem:cha230:col-swap},
  applying the same swaps to the entries of $x$,
  maintains the product between the matrix and the vector,
  so it follows that $A'x = A\tix$.
  \par
  $\impliedby$)
  By assumption, $A'$ is obtained from $A$ by applying the swaps in original order.
  When we apply the same swaps to $\tix$,
  we obtain $x$ and we maintain the product with the matrix by \autoref{rem:cha230:col-swap}.
\end{proof}

\begin{remark}
  \label{rem:cha230:rank-maint}
  Elementary row operations and swapping of columns do not change the rank of a matrix.
\end{remark}
\begin{proof}
  Exercise.
\end{proof}

\section{Consecutive Reduced Row Echelon Form}
  
\begin{definition}
  \label{def:cha230:crref}
  Let $\field$ be a field and $C \in \field^{d \times n}$ for some $d,n \in \NN$.
  We say that $C$ is in \term{consecutive reduced row echelon form} (CRREF)
  if there are $r, k \in \NN$ with $n = r+k$
  and $Z \in \field^{r \times k}$ such that:
  \begin{IEEEeqnarray*}{c}
    C =
    \begin{bmatrix}
      1 & & & & & Z_{1,1} & \hdots & \hdots & \hdots & Z_{1,k} \\
      & \ddots & & \text{\LARGE$0$} & & \vdots & & & & \vdots \\
      & & \ddots & & & \vdots & & & & \vdots \\
      & \text{\LARGE$0$} & & \ddots & & \vdots & & & & \vdots \\
      & & & & 1 & Z_{r,1} & \hdots & \hdots & \hdots & Z_{r,k} \\
      & & & & & & & & & \\
      & & \text{\LARGE$0$} & & & & & \text{\LARGE$0$} & & \\
      & & & & & & & & &
    \end{bmatrix}
  \end{IEEEeqnarray*}
  That is, we have the identity matrix $E_r$ alongside the matrix $Z$.
  At the bottom, all remaining $d-r$ rows have all entries~$0$.\qed
\end{definition}

\begin{remark}
  \label{rem:cha230:dim-ker-C}
  In the context of \autoref{def:cha230:crref}, we have $\rank(C) = r$ and $\dim(\ker(C)) = k$.
\end{remark}
\begin{proof}
  The row space is the span of the first $r$ rows.
  The set of those rows is linearly independent due to the $E_r$ part.
  Hence the dimension of the row space and so the rank of the matrix is~$r$.
  The statement about the kernel follows from \autoref{cor:cha220:dimension-formula}.
\end{proof}

\begin{proposition}
  \label{prop:cha230:basis-ker}
  Let the context of \autoref{def:cha230:crref} be given.
  The set consisting of the columns of the following matrix is a basis of $\ker(C)$:
  \begin{IEEEeqnarray*}{c}
    B \df
    \begin{bmatrix}
      - Z_{1,1} & \hdots & \hdots & \hdots & - Z_{1,k} \\
      \vdots & & & & \vdots \\
      \vdots & & & & \vdots \\
      \vdots & & & & \vdots \\
      - Z_{r,1} & \hdots & \hdots & \hdots & - Z_{r,k} \\
      1\\
      &\ddots\\
      &\!\!\text{\LARGE$0$}&\ddots&\,\,\text{\LARGE$0$}\\
      &&& \ddots \\
      &&&& 1
    \end{bmatrix}
    \in \field^{n \times k}
  \end{IEEEeqnarray*}
\end{proposition}
\begin{proof}
  Denote $\cB \df \seti{B_{\ast,i}}{k}$ and $W \df \spann(\cB) \subseteq \field^n$.
  Due to the $E_k$ part at the bottom, $\cB$ is linearly independent,
  hence $\dim(W) = k$.
  By \autoref{thm:cha210:sub-dim} and \autoref{rem:cha230:dim-ker-C},
  it suffices to show $W \subseteq \ker(C)$,
  for which it suffices to show $\cB \subseteq \ker(C)$.
  \par
  Let $i \in \setn{k}$.
  We prove $B_{\ast,i} \in \ker(C)$, that is, $(C B_{\ast,i})_t = 0$ for all $t \in \setn{d}$.
  It is clear for $r < t \leq d$, since for such $t$ the $t$th row of $C$ consists only of zeros.
  Hence assume $t \leq r$.
  Then we have $(C B_{\ast,i})_t = 1 \cdot (-Z_{t,i}) + Z_{t,i} \cdot 1 = 0$.
\end{proof}

\begin{algorithm}
  \caption{Computing Consecutive Reduced Row Echelon Form}\label{alg:cha230:crref}
  \KwIn{$A \in \field^{d \times n}$, which will be modified during the algorithm if necessary}
  $j \df 1$\;
  \While{$j \leq \min\set{d,n}$}{%
    \If{$\exists i \geq j \holds A_{i,j} \neq 0$}{%
      swap row $i$ with row $j$\;
      scale row $j$ by $A_{j,j}^{-1}$, so we achieve $A_{j,j}=1$\;
      add multiples of row $j$ to other rows as necessary to make all entries in column $j$ to zero,
      except entry $(j,j)$\;
      $j \df j + 1$\;
    }
    \lElseIf{$\exists i \geq j, t > j \holds A_{i,t} \neq 0$}{swap columns $j$ and $t$}
    \lElse{break}
  }
  \Return{$A$}\;
\end{algorithm}

\begin{proposition}
  \autoref{alg:cha230:crref} terminates and delivers a matrix in CRREF
  that results from the input matrix by elementary row operations and column swaps.
\end{proposition}
\begin{proof}
  In each iteration, $j$ is incremented
  or it is ensured, by a column swap, that $j$ will be incremented in the next iteration.
  Hence the algorithm is guaranteed to terminate.
  At any point, all the columns $A_{\ast,t}$ with $t < j$ are of the correct form already.
  This is so since whenever $j$ is going to be incremented,
  column $j$ is brought into the correct form.
  This is done without changing the columns left of it,
  since those columns only contain zeros in entries with an index of $j$ or larger.
  \par
  If the algorithm terminates because $j > d$ or $j > n$,
  it is clear that the matrix is in CRREF:
  if $j > d$, then the diagonal of $1$s stretches to the bottom of the matrix,
  and everything right of it belongs to the matrix $Z$.
  If $j > n$, then the diagonal of $1$s stretches to right end of the matrix,
  and there is no $Z$ part at all.
  \par
  If the algorithm terminates because the break statement in the loop has been reached,
  we know that $A_{i,t} = 0$ for all $i \geq j$ and all $t \geq j$.
  Since the part left of column $j$ only contains the diagonal of $1$s,
  starting from $(1,1)$ and ending at $(j-1,j-1)$, the CRREF is reached.
\end{proof}

\section{Solving an SLE}

\begin{theorem}
  \label{prop:cha230:solution}
  Let $(A,b)$ be an SLE with $d$ equations and $n$ variables over a field~$\field$.
  Let $A'$ result from $A$ by using \autoref{alg:cha230:crref}
  and let $b'$ result from $b$ by performing the same elementary row operations on $b$ as on $A$.
  Let $r$, $k$, and $Z$ be as in \autoref{def:cha230:crref} for $A'$ in place of $C$ there.
  Then the following holds:
  \begin{enumerate}
  \item If there is $i \geq r+1$ with $b'_i \neq 0$, then the SLE is non\-/solvable.
  \item If $b'_i = 0$ for each $i \geq r+1$, then the SLE is solvable.
    A solution is obtained by applying the column swaps that we used to obtain $A'$
    as entry swaps and in reverse order
    to the following vector:
    \begin{IEEEeqnarray*}{c}
      u' \df \RowVec{b'_1 , \VecDots , b'_r , 0 , \VecDots , 0}\tran \in \field^n
    \end{IEEEeqnarray*}
  \item A basis of $\ker(A)$ is obtained by applying the column swaps that we used to obtain $A'$
    as row swaps and in reverse order to the matrix $B$ from \autoref{prop:cha230:basis-ker}.
  \end{enumerate}
\end{theorem}
\begin{proof}
  \begin{enumerate}
  \item By \autoref{prop:cha230:reverse-col-swaps}, if $(A,b)$ is solvable, then also $(A',b')$ is solvable.
    The latter is clearly not the case since $(A'x)_i = 0$ for all $x \in \field^n$ and all $i \geq r+1$.
  \item It is easy to see that $A'u'=b'$.
    The claim follows from \autoref{prop:cha230:row-op} and \autoref{prop:cha230:reverse-col-swaps}.
  \item Follows from \autoref{prop:cha230:basis-ker}, \autoref{prop:cha230:row-op}, and \autoref{prop:cha230:reverse-col-swaps}.\qedhere
  \end{enumerate}
\end{proof}

In practice, when solving an SLE $(A,b)$ with $d$ equations and $n$ variables using \enquote{pen and paper},
it may be notationally more convenient to work the the \term{extended coefficient matrix} $\extcoeff{A}{b}$.
We apply a variation of \autoref{alg:cha230:crref} to this matrix,
where column swaps are only conducted within the first $n$ columns,
that is, in the \enquote{$A$ part} of the matrix.
The result is a matrix of the following form,
which we call the \term{CRREF for extended coefficient matrices}:\footnote{%
  It deviates from the common CRREF only when some of the values $b'_{r+1},\hdots,b'_d$ are non\-/zero,
  that is, in the non\-/solvable case.}
\begin{IEEEeqnarray*}{c}
  \begin{bmatrix}
    1 & & & & & Z_{1,1} & \hdots & \hdots & \hdots & Z_{1,k} & b'_1 \\
    & \ddots & & \text{\LARGE$0$} & & \vdots & & & & \vdots & \vdots \\
    & & \ddots & & & \vdots & & & & \vdots & \vdots \\
    & \text{\LARGE$0$} & & \ddots & & \vdots & & & & \vdots & \vdots \\
    & & & & 1 & Z_{r,1} & \hdots & \hdots & \hdots & Z_{r,k} & b'_r \\
    & & & & & & & & & & b'_{r+1} \\
    & & \text{\LARGE$0$} & & & & & \text{\LARGE$0$} & & & \vdots \\
    & & & & & & & & & & b'_d
  \end{bmatrix}
\end{IEEEeqnarray*}
If there are any colum swaps, we write them down for later.
We decide whether the SLE is solvable by looking at the lower right corner:
if any of the values $b'_{r+1},\hdots,b'_d$ is non\-/zero, then the SLE is non\-/solvable.
Otherwise, the SLE is solvable.
The solution space of $(A',b')$, where $A'$ denotes the \enquote{$A$ part} of the transformed matrix,
is given by:
\begin{IEEEeqnarray*}{c}
  \cL(A',b')
  =
  \ColVec{b'_1 , \VecDots , b'_r , 0 , 0 , 0 , \VecDots , 0}
  +
  \spann\parens[big]{%
    \ColVec{-Z_{1,1} , \VecDots , -Z_{r,1} , 1 , 0 , 0 , \VecDots , 0},%
    \ColVec{-Z_{1,2} , \VecDots , -Z_{r,2} , 0 , 1 , 0 , \VecDots , 0},%
    \hdots,%
    \ColVec{-Z_{1,k} , \VecDots , -Z_{r,k} , 0 , 0 , 0 , \VecDots , 1}%
  }
  \subseteq \field^n
\end{IEEEeqnarray*}
The solution space $\cL(A,b)$ is obtained from this by swapping the entries
in each of those $k+1$ column vectors as per the column swaps that we wrote down earlier,
but now in reverse order.

\begin{example}
  Let the SLE $(A,b)$ over $\RR$ be defined by:
  \begin{IEEEeqnarray*}{c+c}
    A \df
    \begin{bmatrix}
      20 & 28 & 18 & 22 & 15\\
      21 & 27 & 15 & 25 & 19\\
      18 & 22 & 11 & 39 & 22
    \end{bmatrix}
    &
    b \df \bigcolvec{16\\17\\23}
  \end{IEEEeqnarray*}
  The extended coefficient matrix, which is a  $(3 \times 6)$ matrix, is brought into CRREF,
  while we keep track of any column swaps:
  \begin{IEEEeqnarray*}{l"s}
    \begin{gmatrix}[b]
      20 & 28 & 18 & 22 & 15 & 16\\
      21 & 27 & 15 & 25 & 19 & 17\\
      18 & 22 & 11 & 39 & 22 & 23
      \rowops
      \mult{0}{\cdot \tfrac{1}{20}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & \tfrac{7}{5} & \tfrac{9}{10} & \tfrac{11}{10} & \tfrac{3}{4} & \tfrac{4}{5}\\
      21 & 27 & 15 & 25 & 19 & 17\\
      18 & 22 & 11 & 39 & 22 & 23
      \rowops
      \add[-21]{0}{1}
      \add[-18]{0}{2}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & \tfrac{7}{5} & \tfrac{9}{10} & \tfrac{11}{10} & \tfrac{3}{4} & \tfrac{4}{5}\\
      0 & -\tfrac{12}{5} & -\tfrac{39}{10} & \tfrac{19}{10} & \tfrac{13}{4} & \tfrac{1}{5}\\
      0 & -\tfrac{16}{5} & -\tfrac{26}{5} & \tfrac{96}{5} & \tfrac{17}{2} & \tfrac{43}{5}
      \rowops
      \mult{1}{\cdot -\tfrac{5}{12}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & \tfrac{7}{5} & \tfrac{9}{10} & \tfrac{11}{10} & \tfrac{3}{4} & \tfrac{4}{5}\\
      0 & 1 & \tfrac{13}{8} & -\tfrac{19}{24} & -\tfrac{65}{48} & -\tfrac{1}{12}\\
      0 & -\tfrac{16}{5} & -\tfrac{26}{5} & \tfrac{96}{5} & \tfrac{17}{2} & \tfrac{43}{5}
      \rowops
      \add[-\tfrac{7}{5}]{1}{0}
      \add[\tfrac{16}{5}]{1}{2}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 & -\tfrac{11}{8} & \tfrac{53}{24} & \tfrac{127}{48} & \tfrac{11}{12}\\
      0 & 1 & \tfrac{13}{8} & -\tfrac{19}{24} & -\tfrac{65}{48} & -\tfrac{1}{12}\\
      0 & 0 & 0 & \tfrac{50}{3} & \tfrac{25}{6} & \tfrac{25}{3}
      \rowops
    \end{gmatrix}&column swap $(3,4)$\\
    \begin{gmatrix}[b]
      1 & 0 & \tfrac{53}{24} & -\tfrac{11}{8} & \tfrac{127}{48} & \tfrac{11}{12}\\
      0 & 1 & -\tfrac{19}{24} & \tfrac{13}{8} & -\tfrac{65}{48} & -\tfrac{1}{12}\\
      0 & 0 & \tfrac{50}{3} & 0 & \tfrac{25}{6} & \tfrac{25}{3}
      \rowops
      \mult{2}{\cdot \tfrac{3}{50}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 & \tfrac{53}{24} & -\tfrac{11}{8} & \tfrac{127}{48} & \tfrac{11}{12}\\
      0 & 1 & -\tfrac{19}{24} & \tfrac{13}{8} & -\tfrac{65}{48} & -\tfrac{1}{12}\\
      0 & 0 & 1 & 0 & \tfrac{1}{4} & \tfrac{1}{2}
      \rowops
      \add[\tfrac{19}{24}]{2}{1}
      \add[-\tfrac{53}{24}]{2}{0}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 & 0 & -\tfrac{11}{8} & \tfrac{67}{32} & -\tfrac{3}{16}\\
      0 & 1 & 0 & \tfrac{13}{8} & -\tfrac{37}{32} & \tfrac{5}{16}\\
      0 & 0 & 1 & 0 & \tfrac{1}{4} & \tfrac{1}{2}
    \end{gmatrix}
  \end{IEEEeqnarray*}
  We observe $\rank(A) = 3$ and $\dim(\ker(A))=2$. We have:
  \begin{IEEEeqnarray*}{c}
    \cL(A',b') = \colvec{-\tfrac{3}{16}\\\tfrac{5}{16}\\\tfrac{1}{2}\\0\\0}
    + \spann\parens[big]{\colvec{\tfrac{11}{8}\\-\tfrac{13}{8}\\0\\1\\0}, 
      \colvec{-\tfrac{67}{32}\\\tfrac{37}{32}\\-\tfrac{1}{4}\\0\\1}}
  \end{IEEEeqnarray*}
  In order to obtain $\cL(A,b)$ we have to do the swap $(3,4)$:
  \begin{IEEEeqnarray*}{c}
    \cL(A,b) = \colvec{-\tfrac{3}{16}\\\tfrac{5}{16}\\0\\\tfrac{1}{2}\\0}
    +\spann\parens[big]{\colvec{\tfrac{11}{8}\\-\tfrac{13}{8}\\1\\0\\0},
      \colvec{-\tfrac{67}{32}\\\tfrac{37}{32}\\0\\-\tfrac{1}{4}\\1}}
  \end{IEEEeqnarray*}
  Of course, this can also be written like this:
  \begin{IEEEeqnarray*}{c+x*}
    \cL(A,b) = \set[big]{\colvec{-\tfrac{3}{16}\\\tfrac{5}{16}\\0\\\tfrac{1}{2}\\0}
      +\lam_1 \colvec{\tfrac{11}{8}\\-\tfrac{13}{8}\\1\\0\\0}
      + \lam_2 \colvec{-\tfrac{67}{32}\\\tfrac{37}{32}\\0\\-\tfrac{1}{4}\\1}
      \suchthat \lam_1, \lam_2 \in \RR}
    &\qedarray{4}%
  \end{IEEEeqnarray*}
\end{example}
\par\smallskip

\begin{example}
  Let the SLE $(A,b)$ over $\RR$ be defined by:
  \begin{IEEEeqnarray*}{c+c}
    A \df
    \begin{bmatrix}
      13 & 4 & 12 &  6 & 7 \\
      8  & 1 &  6 &  2 & 3 \\
      2  & 1 &  2 &  0 & 3 \\
      9  & 6 & 12 & 12 & 3  
    \end{bmatrix}
    &
    b \df \bigcolvec{3\\2\\0\\3}
  \end{IEEEeqnarray*}
  The extended coefficient matrix, which is a  $(4 \times 6)$ matrix, is brought into CRREF,
  where in this example, we do not require any column swaps:
  \begin{IEEEeqnarray*}{l"s}
    \begin{gmatrix}[b]
      13 & 4 & 12 &  6 & 7 & 3 \\
      8  & 1 &  6 &  2 & 3 & 2 \\
      2  & 1 &  2 &  0 & 3 & 0 \\
      9  & 6 & 12 & 12 & 3 & 3  
      \rowops
      \mult{2}{\cdot \tfrac{1}{2}}
      \swap{0}{2}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1  & \tfrac{1}{2} &  1 &  0 & \tfrac{3}{2} & 0 \\
      8  & 1 &  6 &  2 & 3 & 2 \\
      13 & 4 & 12 &  6 & 7 & 3 \\
      9  & 6 & 12 & 12 & 3 & 3  
      \rowops
      \add[-8]{0}{1}
      \add[-13]{0}{2}
      \add[-9]{0}{3}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1  & \tfrac{1}{2} &  1 &  0 & \tfrac{3}{2} & 0 \\
      0  & -3 & -2 & 2 & -9 & 2 \\
      0  & -\tfrac{5}{2} & -1 &  6 & -\tfrac{25}{2} & 3 \\
      0  & \tfrac{3}{2} & 3 & 12 & -\tfrac{21}{2} & 3  
      \rowops
      \mult{1}{\cdot -\tfrac{1}{3}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1  & \tfrac{1}{2} &  1 &  0 & \tfrac{3}{2} & 0 \\
      0  & 1 & \tfrac{2}{3} & -\tfrac{2}{3} & 3 & -\tfrac{2}{3} \\
      0  & -\tfrac{5}{2} & -1 &  6 & -\tfrac{25}{2} & 3 \\
      0  & \tfrac{3}{2} & 3 & 12 & -\tfrac{21}{2} & 3  
      \rowops
      \add[-\tfrac{1}{2}]{1}{0}
      \add[\tfrac{5}{2}]{1}{2}
      \add[-\tfrac{3}{2}]{1}{3}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1  & 0 & \tfrac{2}{3} & \tfrac{1}{3} & 0 & \tfrac{1}{3} \\
      0  & 1 & \tfrac{2}{3} & -\tfrac{2}{3} & 3 & -\tfrac{2}{3} \\
      0  & 0 & \tfrac{2}{3} & \tfrac{13}{3} & -5 & \tfrac{4}{3} \\
      0  & 0 & 2 & 13 & -15 & 4
      \rowops
      \mult{2}{\cdot \tfrac{3}{2}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1  & 0 & \tfrac{2}{3} & \tfrac{1}{3} & 0 & \tfrac{1}{3} \\
      0  & 1 & \tfrac{2}{3} & -\tfrac{2}{3} & 3 & -\tfrac{2}{3} \\
      0  & 0 & 1 & \tfrac{13}{2} & -\tfrac{15}{2} & 2 \\
      0  & 0 & 2 & 13 & -15 & 4
      \rowops
      \add[-\tfrac{2}{3}]{2}{0}
      \add[-\tfrac{2}{3}]{2}{1}
      \add[-2]{2}{3}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1  & 0 & 0 & -4 & 5 & -1 \\
      0  & 1 & 0 & -5 & 8 & -2 \\
      0  & 0 & 1 & \tfrac{13}{2} & -\tfrac{15}{2} & 2 \\
      0  & 0 & 0 & 0 & 0 & 0
      \rowops
    \end{gmatrix}
  \end{IEEEeqnarray*}
  We observe $\rank(A) = 3$ and $\dim(\ker(A))=2$. We have:
  \begin{IEEEeqnarray*}{c+x*}
    \cL(A,b) = \colvec{-1\\-2\\2\\0\\0}
    +\spann\parens[big]{\colvec{4\\5\\-\tfrac{13}{2}\\1\\0},
      \colvec{-5\\-8\\\tfrac{15}{2}\\0\\1}}
    &\qedarray{4}%
  \end{IEEEeqnarray*}
\end{example}

%%% Local Variables:
%%% TeX-master: "BasicMath.tex"
%%% End:
