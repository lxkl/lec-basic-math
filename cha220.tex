\chapter{Linear Maps}
\label{cha:cha220}

\section{Fundamentals}

\begin{definition}
  Let $V, W$ be vector spaces over the same field $\field$.
  A function ${\phi \in \fnset{V}{W}}$ is called a \term{linear map} or a \term{vector space homomorphism}
  if the following two conditions hold:
  \begin{itemize}
  \item $\phi(x + y) = \phi(x) + \phi(y)$ for all $x,y \in V$ (called \term{additivity})
  \item $\phi(\al x) = \al \phi(x)$ for all $\al \in \field$ and all $x \in V$ (called \term{homogeneity})
    \qed
  \end{itemize}
\end{definition}

\begin{remark}
  Let $V, W$ be vector spaces over the same field $\field$,
  and let ${\phi \in \fnset{V}{W}}$ be a linear map.
  Let $\seti{\al_i}{r} \subseteq \field$ and $\seti{x_i}{r} \subseteq V$ for some $r \in \NN$.
  Then:
  \begin{IEEEeqnarray*}{c}
    \phi\parens{\sum_{i=1}^r \al_i x_i} = \sum_{i=1}^r \al_i \phi(x_i)
  \end{IEEEeqnarray*}
\end{remark}
\begin{proof}
  Follows inductively from the definition.
\end{proof}

\begin{remark}
  \label{rem:cha220:lin-map-comb}
  \begin{enumerate}
  \item Let $V, W$ be vector spaces over the same field $\field$,
    and let ${\phi, \psi \in \fnset{V}{W}}$ be linear maps.
    Let $\lam \in \field$.
    Then $\lam \phi$ and $\phi + \psi$ are linear.
  \item Let $U, V, W$ be vector spaces over the same field $\field$,
    and let ${\phi \in \fnset{U}{V}}$ and ${\psi \in \fnset{V}{W}}$ be linear maps.
    Then $\psi \circ \phi$ is linear.
  \end{enumerate}
\end{remark}
\begin{proof}
  Follows from easy computations.
\end{proof}

\begin{definition}
  Let $V, W$ be vector spaces over the same field $\field$,
  and let ${\phi \in \fnset{V}{W}}$ be a linear map.
  The \term{kernel} or \term{null space} of $\phi$ is:
  \begin{IEEEeqnarray*}{c+x*}
    \ker(\phi) \df \set{ v \in V \suchthat \phi(v)=0 } & \qed
  \end{IEEEeqnarray*}
\end{definition}

\begin{proposition}
  \label{prop:cha220:basics}
  Let $V, W$ be vector spaces over the same field $\field$,
  and ${\phi \in \fnset{V}{W}}$ be a linear map.
  Let $S \leq V$ and $T \leq W$.
  Let $U \subseteq V$ be finite.
  Then the following holds:
  \begin{enumerate}
  \item\label{prop:cha220:basics:1} $\phi(x-y) = \phi(x) - \phi(y)$ for all $x,y \in V$.
  \item\label{prop:cha220:basics:2} $0 \in \ker(\phi)$, that is, $\phi(0) = 0$.
  \item\label{prop:cha220:basics:3} $\fnimg{\phi}(S) \leq W$, in particular $\img(\phi) \leq W$.
  \item\label{prop:cha220:basics:4} $\fnpre{\phi}(T) \leq V$, in particular $\ker(\phi) \leq V$.
  \item\label{prop:cha220:basics:5} $\ker(\phi) = \set{0}$ if and only if $\phi$ is injective.
    (Since $\set{0}$ is the only vector space with dimension $0$, we can also say:
    $\dim(\ker(\phi)) = 0$ if and only if $\phi$ is injective.)
  \item\label{prop:cha220:basics:6} Let $\phi$ be injective.
    Then $U$ is linearly independent (in~$V$) if and only if $\fnimg{\phi}(U)$ is linearly independent (in~$W$).
  \item\label{prop:cha220:basics:7} $\spann(\fnimg{\phi}(U)) = \fnimg{\phi}(\spann(U))$
  \item\label{prop:cha220:basics:8} If $\phi$ is injective, then $\phi^{-1} \in \fnset{\img(\phi)}{V}$ is linear.
  \end{enumerate}
\end{proposition}
\begin{proof}
  \begin{enumerate}
  \item We have:
    \begin{IEEEeqnarray*}{rCl"s+x*}
      \phi(x-y) & = & \phi(x + (-y)) & notation \\
      & = & \phi(x) + \phi(-y) & additivity of $\phi$ \\
      & = & \phi(x) + \phi((-1) y) & by \autoref{prop:cha210:rules} \\
      & = & \phi(x) + (-1) \phi(y) & homogeneity of $\phi$ \\
      & = & \phi(x) + (-\phi(y)) & by \autoref{prop:cha210:rules} \\
      & = & \phi(x) - \phi(y) & notation
    \end{IEEEeqnarray*}
  \item We have $\phi(0) = \phi(0+0) = \phi(0) + \phi(0)$.
    Subtracting $\phi(0)$ from both sides gives $0=\phi(0)$.
  \item Since $S$ is a subspace, $0 \in S$.
    By \ref*{prop:cha220:basics:2}, it follows $0 \in \fnimg{\phi}(S)$.
    Let $x, y \in \fnimg{\phi}(S)$ and $\al \in \field$.
    Then there are $x', y' \in S$ with $\phi(x') = x$ and $\phi(y') = y$.
    It follows $\phi(x' + y') = \phi(x') + \phi(y') = x + y$,
    hence $x' + y'$ is a preimage of $x+y$ under~$\phi$.
    Since $S$ is closed under addition, $x' + y' \in S$,
    hence this preimage is in $S$.
    It follows $x+y \in \fnimg{\phi}(S)$.
    Likewise, $\al x' \in S$ and $\phi(\al x') = \al \phi(x') = \al x$,
    that is, $\al x'$ is a preimage of $\al x$ under $\phi$ in $S$.
    Hence, in total, $\fnimg{\phi}(S)$ contains $0$ and is closed under addition and scalar multiplication,
    thus $\fnimg{\phi}(S) \leq W$.
    The statement about the image follows from the special case $S = V$.
  \item Exercise.
  \item $\implies$) Let $x, y \in V$ with $\phi(x) = \phi(y)$.
    We have to show $x = y$.
    By \ref*{prop:cha220:basics:1}, we have $\phi(x-y) = \phi(x) - \phi(y) = 0$,
    hence $x-y \in \ker(\phi)$.
    It follows $x-y = 0$, thus $x=y$.
    \par
    $\impliedby$) By \ref*{prop:cha220:basics:2}, we know $0 \in \ker(\phi)$.
    We show that this is the only element in this set.
    Let $x \in \ker(\phi)$.
    Then $\phi(x+x) = \phi(x) + \phi(x) = 0 + 0 = 0 = \phi(x)$.
    By injectivity, $x+x = x$, hence $x = 0$.
  \item Let $\phi$ be injective.
    The case $U = \emptyset$ is clear, so assume $U \neq \emptyset$ when necessary.
    \par
    $\implies$) Let $\lam \in \fnfield{\fnimg{\phi}(U)}$ such that $\sum_{w \in \fnimg{\phi}(U)} \lam_w w = 0$.
    Instead of running over all $w \in \fnimg{\phi}(U)$, we can, due to injectivity, also run over all $u \in U$
    and use $\phi(u)$ in place of $w$.
    It follows ${\sum_{u \in U} \lam_{\phi(u)} \phi(u) = 0}$,
    hence by linearity $\phi\parens{\sum_{u \in U} \lam_{\phi(u)} u} = 0$.
    By injectivity and \ref*{prop:cha220:basics:5}, this implies $\sum_{u \in U} \lam_{\phi(u)} u = 0$.
    By linear independence of $U$, it follows $\lam = 0$.
    \par
    $\impliedby$)
    Write $U = \seti{u_i}{r}$ as an indexed set for an appropriate $r \in \NN$,
    and let $w_i \df \phi(u_i)$ for each~$i$.
    Then $\seti{w_i}{r}$ is an indexed set, due to injectivity of~$\phi$,
    and it is equal to $\fnimg{\phi}(U)$.
    Let $\lam \in \field^r$ such that $\sum_{i=1}^r \lam_i u_i = 0$.
    Using linearity, it follows $\sum_{i=1}^r \lam_i \phi(u_i) = 0$,
    meaning $\sum_{i=1}^r \lam_i w_i = 0$.
    By linear independence of $\fnimg{\phi}(U)$, it follows $\lam = 0$.
  \item 
    $\subseteq$)
    Denote $X \df \fnimg{\phi}(U)$.
    For each $x \in X$, let $u_x \in U$ be some pre\-/image of $x$ under $\phi$,
    and denote $U' \df \set{u_x \suchthat x \in X}$.
    Let $y \in \spann(X)$.
    Then there is $\lam \in \fnfield{X}$ such that:
    \begin{IEEEeqnarray*}{c}
      y = \sum_{x \in X} \lam_x x
      = \sum_{u \in U'} \lam_{\phi(u)} \phi(u)
      = \phi\parens{\sum_{u \in U'} \lam_{\phi(u)} u}
    \end{IEEEeqnarray*}
    Since $\sum_{u \in U'} \lam_{\phi(u)} u \in \spann(U') \subseteq \spann(U)$, it follows $y \in \fnimg{\phi}(\spann(U))$.
    \par
    $\supseteq$)
    Let $y \in \fnimg{\phi}(\spann(U))$.
    Then there is $v \in \spann(U)$ with $\phi(v) = y$.
    Let $\lam \in \fnfield{U}$ be such that $v = \sum_{u \in U} \lam_u u$.
    It follows:
    \begin{IEEEeqnarray*}{c}
      y = \phi(v)
      = \phi\parens{\sum_{u \in U} \lam_u u}
      = \sum_{u \in U} \lam_u \phi(u)
      \in \spann(\fnimg{\phi}(U))
    \end{IEEEeqnarray*}
  \item Exercise.\qedhere
  \end{enumerate}
\end{proof}

\section{Isomorphism}

\begin{definition}
  Let $V, W$ be vector spaces over the same field $\field$,
  and let ${\phi \in \fnset{V}{W}}$ be a linear map that is \emphasis{bijective} between $V$ and~$W$.
  Then $\phi$ is called an \term{isomorphism} and $V$ and $W$ are called \term{isomorphic},
  in symbols $V \cong W$.
  By \autoref{prop:cha220:basics}~\ref{prop:cha220:basics:8}, this is equivalent to~$W \cong V$.
  \qed
\end{definition}

Let $V \cong W$ via an isomorphism $\phi \in \fnset{V}{W}$.
Assume that we do not know anything about the addition in $V$ nor about the scalar multiplication of $V$.
Let $x,y \in V$.
We can still compute $x+y$ and $\al x$ in the following way:
\begin{IEEEeqnarray*}{c}
  x + y = \phi^{-1}(\phi(x)) + \phi^{-1}(\phi(y)) = \phi^{-1}(\phi(x) + \phi(y))
\end{IEEEeqnarray*}
Now, in $W$ we can compute $\phi(x) + \phi(y)$,
and then we plug the result into $\phi^{-1}$.
This yields $x+y$ in $V$.
It works similar for scalar multiplication.
Moreover, we have:

\begin{remark}
  Let $V, W$ be vector spaces over the same field $\field$,
  which are isomorphic via an isomorphism $\phi \in \fnset{V}{W}$.
  Let $U \subseteq V$ be finite and $S \subseteq V$. Then:
  \begin{enumerate}
  \item $S \leq V \iff \fnimg{\phi}(S) \leq W$
  \item $U$ is linearly independent (in~$V$) if and only if $\fnimg{\phi}(U)$ is linearly independent (in~$W$).
  \item $U$ is a generating set of $V$ if and only if $\fnimg{\phi}(U)$ is a generating set of $W$.
  \item $U$ is a basis of $V$ if and only if $\fnimg{\phi}(B)$ is a basis of $W$.
    In particular, two isomorphic vector spaces have the same dimension.
  \end{enumerate}
\end{remark}
\begin{proof}
  Follows easily from \autoref{prop:cha220:basics},
  using that $\phi$ is bijective between $V$ and~$W$.
\end{proof}

So, two isomorphic vector spaces are pretty much the same, up to how we refer to their elements.

\begin{definition}
  Let $V$ be a vector space over a field $\field$.
  An indexed set $\seti{b_i}{n} \subseteq V$ for some $n \in \NN$ is called an \term{ordered basis} of $V$
  if it, as a set, is a basis of $V$.\qed
\end{definition}

\begin{theorem}
  \label{thm:cha220:finite-iso}
  Let $n \in \NN$ and $V$ be vector space of dimension $n$ over a field~$\field$.
  Then $V \cong \field^n$.
  Hence up to isomorphism, there is exactly one vector space of dimension $n$ over $\field$.
\end{theorem}
\begin{proof}
  Let $\seti{b_i}{n}$ be an ordered basis of $V$.
  By \autoref{prop:cha210:basis1}, for each $v \in V$,
  there exists a unique $\lam \in \field^n$ with $v = \sum_{i=1}^n \lam_i b_i$.
  Define $\phi(v) \df \lam$.
  Doing this for each $v \in V$ yields a function $\phi \in \fnset{V}{\field^n}$.
  It is now easy to see that this $\phi$ is an isomorphism.
\end{proof}

\section{From Matrices to Linear Maps}
\label{sec:cha220:mat-to-lin}

Recall \autoref{def:cha210:matrix}.
There is an important connection between matrices and linear maps
from $\field^n$ to $\field^d$, which we will present in this and the following section.

\begin{definition}
  \label{def:cha220:mat-mul}
  Let $\field$ be a field.
  Let $d,n,p \in \NN$ and $A \in \field^{d \times n}$ and $B \in \field^{n \times p}$,
  that is, the number of columns of $A$ equals the number of rows of $B$.
  Then we define the product $AB$ as a matrix in $\field^{d \times p}$,
  defined via the rule:
  \begin{IEEEeqnarray*}{c+c+x*}
    (AB)_{i,j} \df \sum_{k=1}^n A_{i,k} B_{k,j} & \forall i \in \setn{d} \quad \forall j \in \setn{p}
    & \qedarray{1}%
  \end{IEEEeqnarray*}
\end{definition}

We can visualize this rule as follows.
In order to compute $(AB)_{i,j}$, we tilt the $j$th column of $B$ by $\SI{90}{\degree}$ counter\-/clockwise
and align it with the $i$th row of~$A$.
Then we multiply each pair of superposed numbers and finally sum up all those products.
As an alternative, we can also imagine the $i$th row of $A$ being tilted $\SI{90}{\degree}$ clockwise
and aligned with the $j$th column of $B$;
then we multiply and sum.
We give an example for a matrix product between the $(3 \times 5)$ matrix from~\eqref{eq:cha210:ex-mat}
and a $(5 \times 2)$ matrix over $\RR$:
\begin{IEEEeqnarray*}{rCl}
  &&
  \begin{bmatrix}
    1 & 0 & 7 & 15 & 1 \\
    3 & 8 & 9 & 7 & 5 \\
    4 & 0 & 1 & 1 & 2
  \end{bmatrix}
  \cdot
  \begin{bmatrix}
    8 & 1 \\ 3 & 0 \\ 2 & 5 \\ 11 & 0 \\ 4 & 7
  \end{bmatrix} \\
  & = &
  \begin{bmatrix}
    1 \cdot 8 + 0 \cdot 3 + 7 \cdot 2 + 15 \cdot 11 + 1 \cdot 4 & 1 \cdot 1 + 0 \cdot 0 + 7 \cdot 5 + 15 \cdot 0 + 1 \cdot 7 \\
    3 \cdot 8 + 8 \cdot 3 + 9 \cdot 2 + 7 \cdot 11 + 5 \cdot 4 &  3 \cdot 1 + 8 \cdot 0 + 9 \cdot 5 + 7 \cdot 0 + 5 \cdot 7 \\
    4 \cdot 8 + 0 \cdot 3 + 1 \cdot 2 + 1 \cdot 11 + 2 \cdot 4 &  4 \cdot 1 + 0 \cdot 0 + 1 \cdot 5 + 1 \cdot 0 + 2 \cdot 7
  \end{bmatrix}\\
  & = &
  \begin{bmatrix}
    191 & 43 \\
    163 & 83 \\
    53 & 23
  \end{bmatrix}
\end{IEEEeqnarray*}
The result is, as expected, a $(3 \times 2)$ matrix.
It is not possible to multiply again with $B$ from the right,
that is, the product $(AB)B$ is not defined.
This is due to a mismatch in the numbers of rows and columns.
However, if $(AB)B$ was defined, it would be the same as $A(BB)$,
that is, matrix multiplication is associative.
We remark this and also a few other rules in a proposition below.

\begin{definition}
  Let $\field$ be a field and $D \in \field^{n \times n}$ for some $n \in \NN$.
  Then $D$ is called a \term{diagonal matrix} if $D_{i,j} = 0$ for all $i,j \in \setn{n}$ with $i \neq j$.
  For each $\al \in \field^n$, we denote by $\diag(\al)$ or $\diag(\eli{\al}{n})$ that diagonal matrix in $\field^{n \times n}$
  that has $\al_i$ as its $(i,i)$th entry for each $i \in \setn{n}$.
  For each $\al \in \field$, we denote by $\diag_n(\al)$ that diagonal matrix in $\field^{n \times n}$
  that has $\al$ in each of its diagonal entries.
  We call $I_n \df \diag_n(1)$ the \term{identity matrix}.
  \qed
\end{definition}

\begin{remark}
  \label{rem:cha220:diag}
  Let $\field$ be a field and $\al \in \field$.
  Let $A \in \field^{d \times n}$ for some $d,n \in \NN$.
  Then $\al A = \diag_d(\al) \cdot A = A \cdot \diag_n(\al)$.
  In particular, $A = I_d A = A I_n$.
\end{remark}
\begin{proof}
  Follows easily from the definitions.
\end{proof}

\begin{proposition}
  \label{rem:cha220:rules}
  Let $\field$ be a field.
  \begin{enumerate}
  \item
    Matrix multiplication is associative,
    that is, for matrices $A,B,C$ over $\field$, we have $(AB)C=A(BC)$,
    provided that all those products are defined.
    The latter is the case if and only if
    $A \in \field^{d \times n}$ and $B \in \field^{n \times p}$ and $C \in \field^{p \times q}$
    for some $d,n,p,q \in \NN$.
  \item
    Matrix multiplication and matrix addition have the distributive property,
    that is, for matrices $A,B,C$ over $\field$, we have $(A+B)C = AC+BC$ and $C(A+B) = CA+CB$,
    each time provided that all the products and sums are defined.
    The latter is the case for $(A+B)C = AC+BC$ if and only if
    $A,B \in \field^{d \times n}$ and $C \in \field^{n \times p}$ for some $d,n,p \in \NN$.
    For $C(A+B) = CA+CB$, it is the case if and only if
    $A,B \in \field^{d \times n}$ and $C \in \field^{p \times d}$ for some $d,n,p \in \NN$.
  \end{enumerate}
\end{proposition}
\begin{proof}
  The statements about definedness are clear from the way addition and multiplication are defined.
  \begin{enumerate}
  \item We have $AB \in \field^{d \times p}$ and thus $(AB) C \in \field^{d \times q}$.
    Let $i \in \setn{d}$ and $j \in \setn{q}$.
    Then by definition of matrix multiplication:
    \begin{IEEEeqnarray*}{c}
      ((AB)C)_{i,j} = \sum_{k=1}^p (AB)_{i,k} C_{k,j}
      = \sum_{k=1}^p \sum_{l=1}^n A_{i,l} B_{l,k} C_{k,j}
    \end{IEEEeqnarray*}
    And:
    \begin{IEEEeqnarray*}{rCl}
      (A(BC))_{i,j}
      &=& \sum_{s=1}^n A_{i,s} (BC)_{s,j}
      = \sum_{s=1}^n A_{i,s} \sum_{t=1}^p B_{s,t} C_{t,j} \\
      &=& \sum_{t=1}^p \sum_{s=1}^n A_{i,s} B_{s,t} C_{t,j}
      = \sum_{k=1}^p \sum_{l=1}^n A_{i,l} B_{l,k} C_{k,j}
    \end{IEEEeqnarray*}
    The last step follows from renaming $t \rightarrow k$ and $s \rightarrow l$.
    We see that $((AB)C)_{i,j} = (A(BC))_{i,j}$.
    Since this holds for all indices $i$ and $j$, it follows $(AB)C = A(BC)$.
  \item For the first statement, we have $A+B \in \field^{d \times n}$ and $(A+B) C \in \field^{d \times p}$
    and $AC + BC \in \field^{d \times p}$.
    Let $i \in \setn{d}$ and $j \in \setn{p}$.
    We have, using the distributive property of $\field$:
    \begin{IEEEeqnarray*}{rCl}
      ((A+B)C)_{i,j} &=& \sum_{k=1}^n (A+B)_{i,k} C_{k,j}
      = \sum_{k=1}^n (A_{i,k} + B_{i,k}) C_{k,j} \\
      &=& \sum_{k=1}^n (A_{i,k} C_{k,j} + B_{i,k} C_{k,j})
      = \sum_{k=1}^n A_{i,k} C_{k,j} + \sum_{k=1}^n B_{i,k} C_{k,j} \\
      &=& (AC)_{i,j} + (BC)_{i,j}
      = (AC + BC)_{i,j}
    \end{IEEEeqnarray*}
    The second statement can be proved likewise.\qedhere
  \end{enumerate}
\end{proof}

\begin{remark}
  Let $\field$ be a field, $\al \in \field$, and $A, B$ be a matrices over $\field$
  such that $AB$ is defined.
  Then $(\al A) B = \al (A B)$.
\end{remark}
\begin{proof}
  Follows from \autoref{rem:cha220:diag} and \autoref{rem:cha220:rules}.
\end{proof}

\begin{proposition}
  \label{prop:cha220:Ax-lin}
  Let $\field$ be a field,
  $A \in \field^{d \times n}$ for some $d,n \in \NN$.
  Recall that for each $x \in \field^n$, we identify $x$ with a column vector,
  that is, a matrix in $\field^{n \times 1}$, hence the product $Ax$ is defined.
  We have:
  \begin{enumerate}
  \item\label{prop:cha220:Ax-lin:1} $Ax = \sum_{k=1}^n x_k A_{\ast,k}$ for all $x \in \field^n$.
  \item\label{prop:cha220:Ax-lin:2} $\phi_A \df \fn{\field^n}{x \mapsto Ax}$ is a linear map from $\field^n$ to $\field^d$.
  \item\label{prop:cha220:Ax-lin:3} $\img(\phi_A) = \spann(\seti{A_{\ast,i}}{n})$ for the linear map $\phi_A$ from the previous item.
  \end{enumerate}
\end{proposition}
\begin{proof}
  \begin{enumerate}
  \item Let $i \in \setn{d}$.
    By entry\-/wise addition and scalar multiplication:
    \begin{IEEEeqnarray*}{rcl}
      \parens{x_k A_{\ast,k}}_i
      &=& \sum_{k=1}^n \parens{x_k A_{\ast,k}}_i
      = \sum_{k=1}^n x_k \parens{A_{\ast,k}}_i \\
      &=& \sum_{k=1}^n x_k A_{i,k}
      = \sum_{k=1}^n A_{i,k} x_{k,1}
      = (Ax)_{i,1}
      = (Ax)_i
    \end{IEEEeqnarray*}
    We have switched explicitly between an $(n \times 1)$ matrix and a column vector of length $n$ here.
    This distinction will typically not be made in the future.
  \end{enumerate}
  \ref*{prop:cha220:Ax-lin:2} and \ref*{prop:cha220:Ax-lin:3} follow from~\ref*{prop:cha220:Ax-lin:1}.
\end{proof}

\begin{remark}
  Let $\field$ be a field,
  $A \in \field^{d \times n}$ and $B \in \field^{n \times p}$ for some $d,n,p \in \NN$.
  Then $\phi_A \circ \phi_B = \phi_{AB}$.
\end{remark}
\begin{proof}
  Follows from the associative property of matrix multiplication.
\end{proof}

\section{From Linear Maps to Matrices}
\label{sec:cha220:lin-to-mat}

\begin{proposition}
  \label{prop:cha220:same-on-basis}
  Let $V, W$ be vector spaces over the same field $\field$,
  and let $V$ be finite\-/dimensional.
  Let $B = \seti{b_i}{n}$ be an ordered basis of $V$
  and ${Y = \seti{y_i}{n} \subseteq W}$.\footnote{%
    Note that since we did not say that $Y$ was an indexed set,
    the map $\fn{\setn{n}}{i \mapsto y_i}$ does not have to be injective.}
  Then there is exactly one linear map ${\phi \in \fnset{V}{W}}$
  under the condition $\phi(b_i) = y_i$ for each $i \in \setn{n}$.
  This map is given by $\phi(x) = \sum_{i=1}^n \lam_i y_i$ for each $x \in V$,
  where $\lam$ is determined via $x = \sum_{i=1}^n \lam_i b_i$,
  that is, we obtain $\phi(x)$ by expressing $x$ in the basis $B$
  and then using the coefficients to form an LC of $Y$.
\end{proposition}
\begin{proof}
  Let $\phi$ be given as described.
  \begin{itemize}
  \item We prove $\phi(b_j) = y_j$ for each $j \in \setn{n}$.
    One way to give $b_j$ as an LC of $B$ is by choosing $\lam_i \df 0$ for each $i \neq j$
    and $\lam_j \df 1$.
    By \autoref{prop:cha210:basis1}, this is the only way to do it,
    hence this $\lam$ is used to define $\phi(x)$ as $\sum_{i=1}^n \lam_i y_i$, which is~$y_j$.
  \item We prove that $\phi$ is linear.
    Let $x,y \in V$.
    Let $\lam, \mu \in \KK^n$ be such that $x = \sum_{i=1}^n \lam_i b_i$ and $y = \sum_{i=1}^n \mu_i b_i$.
    It follows $x + y = \sum_{i=1}^n (\lam_i + \mu_i) b_i$.
    This is an LC yielding $x+y$, again by \autoref{prop:cha210:basis1},
    the coefficients $\rho \df \fn{\setn{n}}{i \mapsto \lam_i + \mu_i}$ are the only way to obtain $x+y$
    as an LC of $B$.
    Note that the sum is indeed arranged by vectors.
    Therefore, $\rho$ was used when defining $\phi(x+y)$, that is,
    $\phi(x+y) = \sum_{i=1}^n \rho_i b_i = \sum_{i=1}^n (\lam_i+\mu_i) b_i
    = \sum_{i=1}^n \lam_i b_i + \sum_{i=1}^n \mu_i b_i = \phi(x) + \phi(y)$.
    The last step uses \autoref{prop:cha210:basis1} once more.
    The homogeneity is proved likewise.
  \item We prove that $\phi$ is the only linear map with this property.
    Let $\psi$ be another such linear map and let $x \in V$.
    Let $\lam \in \field^n$ with $x = \sum_{i=1}^n \lam_i b_i$.
    Then, only by linearity of $\phi$ and $\psi$:
    \begin{IEEEeqnarray*}{rCl+x*}
      \phi(x)
      &=& \phi\parens{\sum_{i=1}^n \lam_i b_i}
      = \sum_{i=1}^n \lam_i \phi(b_i)
      = \sum_{i=1}^n \lam_i y_i \\
      &=& \sum_{i=1}^n \lam_i \psi(b_i)
      = \psi\parens{\sum_{i=1}^n \lam_i b_i}
      = \psi(x)
      &\qedarray[\qedhere]{1}%
    \end{IEEEeqnarray*}
  \end{itemize}
\end{proof}

\begin{remark}
  Let $\field$ be a field and $\phi \in \fnset{\field^n}{\field^d}$ be linear for some $d,n \in \NN$.
  Define $A \df \rowvec{ \phi(e_1) & \hdots & \phi(e_n) }$, where $B = \seti{e_i}{n}$ is the standard basis,
  that is, $A$ is the matrix that has as its $i$th column the image of the $i$th standard basis vector under~$\phi$.
  Then $\phi = \phi_A$.
\end{remark}
\begin{proof}
  For each $i \in \setn{n}$, we have $\phi_A(e_i) = A e_i = A_{\ast,i} = \phi(e_i)$.
  Hence on the basis $B$, the two functions $\phi$ and $\phi_A$ coincide.
  By \autoref{prop:cha220:same-on-basis}, it follows $\phi = \phi_A$.
\end{proof}

In the following, we will no longer distinguish between a matrix $A$ and the linear map $\phi_A$,
hence we consider $A \in \field^{d \times n}$ as being a function from $\field^n$ to $\field^d$.
Thus we can use notation from linear functions, in particular for kernel and image:
\begin{IEEEeqnarray*}{l}
  \ker(A) = \set{x \in \field^n \suchthat Ax = 0} \\
  \img(A) = \set{Ax \suchthat x \in \field^n}
\end{IEEEeqnarray*}

\begin{example}
  \label{ex:cha220:xchange}
  Let the linear map $\phi \in \fnset{\RR^2}{\RR^2}$ be defined via:
  \begin{IEEEeqnarray}{c+c}
    \label{eq:cha220:xchange:1}
    \phi\parens{\colvec{5\\5}} = \colvec{2\\2}
    &
    \phi\parens{\colvec{3\\0}} = \colvec{0\\1}
  \end{IEEEeqnarray}
  This is a complete definition since $\set{(5, 5), (3, 0)}$
  is a basis of $\RR^2$.
  In order to compute $\phi(e_1)$ and $\phi(e_2)$, we express $e_1$ and $e_2$ in this basis:
  \begin{IEEEeqnarray*}{c+c}
    e_1 = \colvec{1\\0} = \tfrac{1}{3} \cdot \colvec{3\\0}
    &
    e_2 = \colvec{0\\1} = \tfrac{1}{5} \cdot \colvec{5\\5} - \tfrac{1}{3} \cdot \colvec{3\\0}
  \end{IEEEeqnarray*}
  The coefficients were obtained by \enquote{taking a close look}.
  It follows:
  \begin{IEEEeqnarray*}{rCl}
    \phi(e_1) & = & \tfrac{1}{3} \cdot \phi\parens{\colvec{3\\0}} = \tfrac{1}{3} \cdot \colvec{0\\1} = \colvec{0\\\tfrac{1}{3}}\\
    \phi(e_2) & = & \tfrac{1}{5} \cdot \phi\parens{\colvec{5\\5}} - \tfrac{1}{3} \cdot \phi\parens{\colvec{3\\0}}
    = \tfrac{1}{5} \cdot \colvec{2\\2} - \tfrac{1}{3} \cdot \colvec{0\\1} = \colvec{\tfrac{2}{5} \\ \tfrac{1}{15}}
  \end{IEEEeqnarray*}
  From this, we obtain the matrix:
  \begin{IEEEeqnarray*}{c}
    \begin{bmatrix}
      0 & \tfrac{2}{5} \\
      \tfrac{1}{3} & \tfrac{1}{15}
    \end{bmatrix}
  \end{IEEEeqnarray*}
  This was not obvious from~\eqref{eq:cha220:xchange:1}.
  In \autoref{cha:cha240}, we will learn about a method to compute such a matrix more systematically.
  \qed
\end{example}

\section{Dimension Formula}

\begin{definition}
  Let $\field$ be a field and $A \in \field^{d \times n}$ for some $d,n \in \NN$.
  Define:
  \begin{itemize}
  \item The \term{row space} $\cR(A) \df \spann(\seti{A_{i,\ast}}{d})$ of $A$.
  \item The \term{column space} $\cC(A) \df \spann(\seti{A_{\ast,i}}{n})$ of $A$.
  \end{itemize}
  The \term{row rank} of $A$ is $\rowrank(A) \df \dim(\cR(A))$,
  and the \term{column rank} of $A$ is $\colrank(A) \df \dim(\cC(A))$.\qed
\end{definition}

\begin{remark}
  \label{rem:cha22:rank}
  Let $A$ be as in the preceding definition. Then we have:
  \begin{enumerate}
  \item\label{rem:cha22:rank:1} $\cR(A) \leq \field^n$, hence $\rowrank(A) \leq n$
  \item\label{rem:cha22:rank:2} $\cC(A) \leq \field^d$, hence $\colrank(A) \leq d$
  \item\label{rem:cha22:rank:3} $\rowrank(A) \leq d$ and $\colrank(A) \leq n$,
    hence with the previous items, $\max\set{\rowrank(A), \colrank(A)} \leq \min\set{d,n}$.
  \item\label{rem:cha22:rank:4} $\cC(A) = \img(A)$
  \end{enumerate}
\end{remark}
\begin{proof}
  \ref*{rem:cha22:rank:1} and \ref*{rem:cha22:rank:2} follow from the definitions and \autoref{thm:cha210:sub-dim}.
  Item~\ref*{rem:cha22:rank:3} follows from \autoref{prop:cha210:iter-1}.
  Item~\ref*{rem:cha22:rank:4} follows from \autoref{prop:cha220:Ax-lin}.
\end{proof}

\begin{theorem}
  \label{thm:cha220:rank}
  Let $\field$ be a field and $A \in \field^{d \times n}$ for some $d,n \in \NN$.
  Then $\rowrank(A) = \colrank(A)$.
\end{theorem}
\begin{proof}
  We start by proving the following \textbf{claim:}
  Removing a row from the matrix that is
  in the span of the other rows does not change the row rank
  nor the column rank.
  \par
  The statement is clear regarding the row rank, so we look at the column rank.
  Let $t \in \setn{d}$ be the index of a row that is in the span of the other rows.
  For each vector $v \in \field^d$ denote by $v' \in \field^{d-1}$ the vector obtained from $v$ by removing the $t$th entry.
  Then the matrix obtained from $A$ by removing row $t$ is $A' \df \rowvec{A'_{\ast,1} & \hdots & A'_{\ast,n}} \in \field^{(d-1) \times n}$.
  By \autoref{prop:cha210:iter-1}, there is a basis $B \subseteq \seti{A_{\ast,i}}{n}$ of $\cC(A)$.
  Define $B' \df \set{b' \suchthat b \in B}$.
  It is easy to see that $\spann(B') = \cC(A')$.
  We prove that moreover, $\card{B'} = \card{B}$ and that $B'$ is linearly independent,
  hence $B'$ is a basis of $\cC(A')$ with the same cardinality as a basis of $\cC(A)$.
  This means $\colrank(A) = \colrank(A')$.
  \par
  To this end, write $B = \setj{b_j}{r}$ as an indexed set for some $r \in \NN$.
  We will prove:
  \begin{IEEEeqnarray}{c}
    \label{eq:cha220:rank:claim}
    \forall \lam \in \field^r \holds \sum_{j=1}^r \lam_j b'_j = 0 \implies \lam = 0
  \end{IEEEeqnarray}
  For one, this implies that $\fn{\setn{r}}{j \mapsto b'_j}$ is injective:
  if there was $j_1 \neq j_1$ with $b'_{j_1} = b'_{j_2}$,
  we could choose $\lam_{j_1} \df 1$, $\lam_{j_2} \df -1$, and $\lam_j \df 0$ for all $j \in\setn{r}\setminus\set{j_1,j_2}$
  and immediately have a counterexample for~\eqref{eq:cha220:rank:claim}.
  This injectivity implies that $\card{B'} = \card{B}$.
  Moreover, \eqref{eq:cha220:rank:claim} implies that $B'$ is linearly independent.
  \par
  Let $\lam \in \field^r$ such that $\sum_{j=1}^r \lam_j b'_j = 0$,
  that is, written entry\-/wise with $I \df \setn{d} \setminus \set{t}$:
  \begin{IEEEeqnarray}{c}
    \label{eq:cha220:rank:1}
    \sum_{j=1}^r \lam_j (b_j)_i = 0 \quad \forall i \in I
  \end{IEEEeqnarray}
  We are done when we know that ${\sum_{j=1}^r \lam_j (b_j)_t = 0}$,
  since this implies ${\sum_{j = 1}^r \lam_j b_j = 0}$ and thus $\lam = 0$ by linear independence of~$B$.
  By assumption, there is ${\mu \in \fnfield{I}}$
  such that $(b_j)_t = \sum_{i \in I} \mu_i (b_j)_i$ for each $j \in \setn{r}$.
  It follows:
  \begin{IEEEeqnarray*}{c}
    \sum_{j=1}^r \lam_j (b_j)_t
    = \sum_{j=1}^r \lam_j \sum_{i \in I} \mu_i (b_j)_i
    = \sum_{i \in I} \mu_i \sum_{j=1}^r \lam_j (b_j)_i
    = \sum_{i \in I} \mu_i \cdot 0
    = 0
  \end{IEEEeqnarray*}
  In the last step, we used~\eqref{eq:cha220:rank:1}.
  So we have proved~\eqref{eq:cha220:rank:claim} and thus the claim.
  Clearly, the same claim holds for removing columns that are in the span of the other columns
  -- we merely swap \enquote{column} and \enquote{row} in the above proof.
  \par
  Let $B \in \field^{p \times q}$ be a matrix obtained from $A$ by successively removing
  rows that are in the span of the other rows
  and columns that are in the span of the other columns,
  as long as at least one row and at least one column remains.
  Then, by the claim, $\rowrank(A) = \rowrank(B)$ and $\colrank(A) = \colrank(B)$,
  so we are done if we can show $\rowrank(B) = \colrank(B)$.
  By construction, each two rows of $B$ are distinct
  and each two columns of $B$ are distinct,
  hence $p$ is the cardinality of the set of rows and $q$ is the cardinality of the set of columns.
  \begin{itemize}
  \item Case 1: The set of rows and the set of columns each are linearly independent.
    Then $\rowrank(B) = p$ and $\colrank(B) = q$.
    At the same time, by \autoref{rem:cha22:rank}, we have $\rowrank(B) \leq q$ and $\colrank(B) \leq p$,
    hence $p \leq q$ and $q \leq p$, which implies $p=q$,
    that is, $\rowrank(B) = \colrank(B)$.
  \item Case 2: The set of rows or the set of columns is \emphasis{not} linearly independent.
    \Wlg assume that the set of columns is not linearly independent.
    By \autoref{prop:cha210:one-is-LC}, this implies $q=1$,
    since otherwise, we would have removed further columns.
    So we have a set with exactly one vector that is not linearly independent.
    By \autoref{prop:cha210:rules}~\ref{prop:cha210:rules:3}, it follows that this vector is the null vector,
    hence $\colrank(B) = 0$.
    We also have $\rowrank(B) = 0$ since the null vector in $\KK^d$ has all its entries zero.
    (By our procedure before, all but one row in this matrix would have been removed,
    so in fact we have $p=1$.)\qedhere
  \end{itemize}
\end{proof}

For completeness, we give a linear independence notion for tuples.
We actually used this notion in~\eqref{eq:cha220:rank:claim}.

\begin{definition}
  Let $V$ be a vector space over a field $\field$.
  Let $r \in \NN$ and $u \in V^r$.
  Then $u$ is called \term{linearly independent} if:
  \begin{IEEEeqnarray*}{c+x*}
    \forall \lam \in \field^r \holds \parens[Big]{ \sum_{i=1}^r \lam_i u_i = 0 \implies \lam = 0 } & \qed
  \end{IEEEeqnarray*}
\end{definition}

If a tuple is linearly independent, then in particular, its entries are pairwise distinct;
and by contraposition, if two entries are the same, then the tuple is \emphasis{not} linearly independent.
This is perhaps the main advantage of this notion, as we have seen in the preceding proof.

\begin{remark}
  Let $V$ be a vector space over a field $\field$, and let $u \in V^r$.
  Then $u$ is linearly independent (as a tuple) if and only if
  $\seti{u_i}{r}$ is linearly independent (as a set) and $\fn{\setn{r}}{i \map u_i}$ is injective.
\end{remark}
\begin{proof}
  $\implies$)
  Linear independence of $\seti{u_i}{r}$ follows directly from the definition.
  Now assume for contradiction that there are $s,t \in \setn{r}$ with $s \neq t$ and $u_s = u_t$.
  Then define:
  \begin{IEEEeqnarray*}{c}
    \lam \df \fn{\setn{r}}{i \mapsto
      \begin{cases}
        -1 & i = s \\
        1 & i = t \\
        0 & \text{otherwise}
      \end{cases}}
  \end{IEEEeqnarray*}
  Then $\sum_{i=1}^r \lam_i u_i = 0$, but $\lam \neq 0$, a contradiction to linear independence of $u$.
  \par
  $\impliedby$)
  Follows since for each $\lam \in \field^r$, the sum $\sum_{i=1}^r \lam_i u_i$ is arranged by vectors.
\end{proof}

Many of the results that we have on linear independence of sets carry over -- with minor adaptions --
to linear independence of tuples.
For example, \autoref{prop:cha210:final-straw} for tuples reads like this:

\begin{proposition}
  \label{prop:cha220:final-straw:tuples}
  Let $V$ be a vector space over a field $\field$, and let $u \in V^r$ be linearly independent.
  Let $x \in V$ be such that $u' \df (\eli{u}{r},x)$ is \emphasis{not} linearly independent.
  Then, $x \in \spann(\eli{u}{r})$.
\end{proposition}
\begin{proof}
  Similar to the proof of \autoref{prop:cha210:final-straw}.
\end{proof}

\begin{definition}
  Let $\field$ be a field and $A \in \field^{d \times n}$ for some $d,n \in \NN$.
  Then the \term{rank} of $A$, denoted by $\rank(A)$,
  is the common value of $\rowrank(A)$ and $\colrank(A)$.
  \qed
\end{definition}

\begin{remark}
  \label{rem:cha220:img-rank}
  Let $\field$ be a field and $A \in \field^{d \times n}$ for some $d,n \in \NN$.
  Then $\rank(A) = \dim(\img(A))$.
\end{remark}
\begin{proof}
  Follows directly from the definitions.
\end{proof}

\begin{proposition}
  \label{prop:cha220:cap-nul}
  Let $V$ be a vector space over a field $\field$.
  Let $U \subseteq V$ be linearly independent and let $U_0, U_1 \subseteq U$ be such that $U_0 \cap U_1 = \emptyset$.
  Then $\spann(U_0) \cap \spann(U_1) = \set{0}$.
\end{proposition}
\begin{proof}
  Clearly, $0$ is in both of the spans.
  Now let $x \in \spann(U_0) \cap \spann(U_1)$.
  Then there are ${\lam\in \fnfield{U_0}}$ and ${\mu\in \fnfield{U_1}}$
  such that $\sum_{u \in U_0} \lam_u u = x = \sum_{u \in U_1} \mu_u u$.
  It follows $\sum_{u \in U_0} \lam_u u - \sum_{u \in U_1} \mu_u u = 0$,
  which is a LC of $U$, which is arranged by vectors since $U_0 \cap U_1 = \emptyset$.
  Hence $\lam=0$ and $\mu=0$, hence $x=0$.
\end{proof}

\begin{theorem}[Dimension Formula]
  \label{thm:cha220:dimension-formula}
  Let $V, W$ be vector spaces over the same field $\field$,
  and let $V$ be finitely generated with dimension $n \df \dim(V)$.
  Let ${\phi \in \fnset{V}{W}}$ be a linear map.
  Then $n = \dim(\ker(\phi)) + \dim(\img(\phi))$.
\end{theorem}
\begin{proof}
  Since $\ker(\phi) \leq V$ and $V$ is finitely generated,
  by \autoref{thm:cha210:sub-dim}, there is a basis $B_0$ of $\ker(\phi)$.
  By \autoref{prop:cha210:iter-2}, there is $B_1 \subseteq V \setminus B_0$ such that $B \df B_0 \cup B_1$ is a basis of $V$,
  and hence in particular $\card{B}=n$.
  Denote $V_1 \df \spann(B_1)$. We will prove the following two claims:
  \begin{itemize}
  \item $\phi$ restricted to $V_1$ is injective.
  \item $\fnimg{\phi}(V_1) = \img(\phi)$
  \end{itemize}
  This implies that $V_1$ and $\img(\phi)$ are isomorphic,
  hence have the same dimension.
  It follows:
  \begin{IEEEeqnarray*}{c}
    n = \card{B} = \card{B_0} + \card{B_1} = \dim(\ker(\phi)) + \dim(V_1) = \dim(\ker(\phi)) + \dim(\img(\phi))
  \end{IEEEeqnarray*}
  Now for the proofs of the claims:
  \begin{itemize}
  \item Let $x,y \in V_1$ be such that $\phi(x) = \phi(y)$.
    By linearity, we have $0 = \phi(x) - \phi(y) = \phi(x-y)$, hence $x-y \in \ker(\phi)$.
    At the same time, since $V_1$ is closed under addition, $x-y \in V_1$.
    By \autoref{prop:cha220:cap-nul}, it follows $x-y = 0$, hence $x=y$.
  \item The includion $\subseteq$ is clear.
    So let $w \in \img(\phi)$.
    Then there is $v \in V$ with $\phi(v) = w$.
    For this $v$, we find $\lam \in \fnfield{B}$ with $v = \sum_{b \in B} \lam_b b$.
    We have by linearity and since $B_0 \subseteq \ker(\phi)$:
    \begin{IEEEeqnarray*}{rCl}
      w &=& \phi(v) = \sum_{b \in B} \lam_b \phi(b) = \sum_{b \in B_1} \lam_b \phi(b) \\
      &\in& \spann(\fnimg{\phi}(B_1)) = \fnimg{\phi}(\spann(B_1)) = \fnimg{\phi}(V_1)
    \end{IEEEeqnarray*}
    We have used \autoref{prop:cha220:basics}~\ref{prop:cha220:basics:7} here.\qedhere
  \end{itemize}
\end{proof}

\begin{corollary}
  \label{cor:cha220:dimension-formula}
  Let $\field$ be a field and $A \in \field^{d \times n}$ for some $d,n \in \NN$.
  Then $n = \dim(\ker(A)) + \rank(A)$.
\end{corollary}
\begin{proof}
  Follows from \autoref{rem:cha220:img-rank} and \autoref{thm:cha220:dimension-formula}.
\end{proof}

\begin{proposition}
  \label{prop:cha22:rank-injectiv-surjektiv}
  Let $\field$ be a field and $A \in \field^{d \times n}$ for some $d,n \in \NN$.
  Then we have:
  \begin{enumerate}
  \item\label{prop:cha22:rank-injectiv-surjektiv:1} $A$ is injective if and only if $\rank(A) = n$.
  \item\label{prop:cha22:rank-injectiv-surjektiv:2} $A$ is surjective on $\field^d$ if and only if $\rank(A) = d$.
  \item\label{prop:cha22:rank-injectiv-surjektiv:3} $A$ is bijective between $\field^n$ and $\field^d$ if and only if $\rank(A) = d = n$.
  \end{enumerate}
\end{proposition}
\begin{proof}
  \begin{enumerate}
  \item We have:
    \begin{IEEEeqnarray*}{rCl"s}
      \text{$A$ is injective}
      & \iff & \dim(\ker(A)) = 0 & by \autoref{prop:cha220:basics}~\ref{prop:cha220:basics:5} \\
      & \iff & n = 0 + \rank(A) & by \autoref{cor:cha220:dimension-formula} \\
      & \iff & \rank(A) = n
    \end{IEEEeqnarray*}
  \item We have:
    \begin{IEEEeqnarray*}{rCl"s}
      \text{$A$ is surjective on $\field^d$} & \iff & \dim(\img(A)) = d & by \autoref{thm:cha210:sub-dim}~\ref{thm:cha210:sub-dim:2} \\
      & \iff & \rank(A) = d & by \autoref{rem:cha220:img-rank}
    \end{IEEEeqnarray*}
  \item Follows directly from \ref*{prop:cha22:rank-injectiv-surjektiv:1}
    and \ref*{prop:cha22:rank-injectiv-surjektiv:2}.
    \qedhere
  \end{enumerate}
\end{proof}

%%% Local Variables:
%%% TeX-master: "BasicMath.tex"
%%% End:
