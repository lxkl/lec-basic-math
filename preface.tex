\chapter*{Preface}

This is work in progress.
The first chapters are still missing.
The first chapter that is currently available (on real and complex numbers)
assumes familiarity with the following topics:
\begin{itemize}
\item sets, functions
\item elementary logic, proofs (direct, indirect, induction)
\item relations, orders
\item semigroups, groups
\item elementary combinatorics, factorial, binomial coefficient, binomial theorem
\end{itemize}

\section*{Numbers}

The natural numbers $\NN = \set{1,2,3,\hdots}$ and the natural numbers with zero $\NNzero = \set{0,1,2,3,\hdots}$
will not be constructed in this book,
but we instead rely on an intuitive understanding.
For each $n \in \NN$, denote $\setn{n} \df \setft{1}{n}$ the set of the first $n$ natural numbers.
In \autoref{cha:cha160}, we will introduce the real numbers $\RR$ axiomatically
and then embed $\NN$ into $\RR$,
which will give us the integers $\ZZ = \set{\hdots, -3, -2, -1, 0, 1, 2, 3, \hdots}$
and the rationals $\QQ$.
Up to \autoref{cha:cha160} (that is, in the chapters still to be written), besides $\NN$,
we will also use $\ZZ$ and $\QQ$ in a few places.
For example, we will use the expression $\frac{n (n+1)}{2}$ for $n \in \NN$.
In another place, we will remark that $\ZZ$ with the common addition operation is a group
(what a \term{group} means will be explained there).
Such can be understood with knowledge from school
about integers, rationals, and their arithmetic.

\section*{Functions}

For any two objects $x$ and $y$, define the \term{pair} $(x,y) \df \set{\set{x}, \set{x,y}}$.
Given two sets $X$ and $Y$, define the set of their pairs as $X \times Y \df \set{(x,y) \suchthat x \in X \land y \in Y}$.
A~subset of $X \times Y$ is called a \term{relation} between $X$ and $Y$.
A~\term{function} from $X$ to $Y$ is a total and right\-/unique relation between $X$ and $Y$,
that is, a set $f \subseteq X \times Y$ is called a \term{function} from $X$ to $Y$
if for each $x \in X$, there is \emphasis{exactly one} $y \in Y$ with $(x,y) \in f$.
We say that $f$ \term{maps} $x$ to this $y$ and denote this $y$ by $f(x)$
or sometimes using the index notation, that is, we denote it by~$f_x$.
Whether to use $f(x)$ or $f_x$ is a matter of taste,
and historically, in certain contexts one is more common than the other.
The set $X$ is called the \term{domain} of the function~$f$ and is denoted by $\dom(f)$.
The set $Y$ is called a \term{codomain} of the function~$f$.
The \term{image} of $f$ is defined as:
\begin{IEEEeqnarray*}{c}
  \img(f) \df \set{y \in Y \suchthat \exists x \in X \holds f(x) = y}
\end{IEEEeqnarray*}
Clearly, any set containing $\img(f)$ as a subset qualifies as a codomain of $f$.
By $\fnset{X}{Y}$, we denote the set of all functions from $X$ to $Y$,
another notation for this is $Y^X$.
Note that if $Y \subseteq Y'$ for some set $Y'$, then $\fnset{X}{Y} \subseteq \fnset{X}{Y'}$.

Let $f \in \fnset{X}{Y}$.
For a set $A \subseteq X$, we denote the \term{image} of $A$ under $f$ as:
\begin{IEEEeqnarray*}{c}
  \fnimg{f}(A) \df \set{ y \in Y \suchthat \exists x \in A \holds f(x) = y } = \set{ f(x) \suchthat x \in A }
\end{IEEEeqnarray*}
Clearly, $\img(f) = \fnimg{f}(\dom(f))$.
We also use the notation $\fnimg{f}(A) = \set{f_x}_{x \in A}$.
If the set $A$ is of the form $A = \setft{a}{b}$ for $a,b \in \ZZ$,
we also write ${\fnimg{f}(A) = \set{f_x}_{x=a}^b}$.

For any set $B$, we denote the \term{preimage} of $B$ under $f$ as:
\begin{IEEEeqnarray*}{c}
  \fnpre{f}(B) \df \set{ x \in X \suchthat f(x) \in B }
\end{IEEEeqnarray*}
When $B = \set{b}$ for some object $b$,
we call the elements of $\fnpre{f}(B)$ the \term{preimages} of $b$ under~$f$.

A function $f$ with domain $\setn{n}$ for some $n \in \NN$ is called a \term{tuple} of \term{length} $n$, or an \xtuple{n},
and $f(i)$ or $f_i$ is called the $i$th \term{entry} or \term{component} of the tuple.
In general, when the domain $\dom(f)$ of a function $f$ is finite, we call $\card{\dom(f)}$ the \term{length} of~$f$.

Several notations for functions exist.
We often use the following, slightly non\-/standard way of denoting functions.\footnote{%
  Our notation is similar to that used in:
  Leslie Lamport, \textit{Specifying Systems: The TLA+ Language and Tools for Hardware and Software Engineers}, 2003,
  Section 5.2.}
Let $X$ be a set.
We write $\fn{X}{x \mapsto \hdots}$ for the function with domain $X$
that maps each $x \in X$
to the object specified by the expression that is given in place of the $\hdots$ (three dots).
A codomain is not specified here,
but we will only consider such expressions in place of $\hdots$ that admit the existence of a codomain,
that is, existence of a set $Y$
such that $f(x) \in Y$ for each $x \in X$.
If the domain is clear from context or does not matter for the argument at hand,
we also write just $(x \mapsto \hdots)$.
If the variable, here $x$, is clear from context, it and the arrow $\mapsto$ may also be omitted.

As an example for a function in our notation, define:
\begin{IEEEeqnarray*}{c}
  \chi \df
  \fn{\RR}{x \mapsto
    \begin{dcases}
      1 & x \in \QQ \\
      0 & \text{otherwise}
    \end{dcases}}
\end{IEEEeqnarray*}
Then $\chi$ is called the \term{characteristic function} of $\QQ$ on $\RR$,
and ${\dom(\chi) = \RR}$.
As another example consider $g \df \fn{\NN}{n \mapsto \frac{1}{n}}$.
In an appropriate context, this function might be written simply as $(n \mapsto \frac{1}{n})$ or~$(\frac{1}{n})$.

Let $f$ be a function.
For a set $A \subseteq \dom(f)$, we define the \term{restriction} of $f$ to $A$ as
$\fnres{f}{A} \df \fn{A}{x \mapsto f(x)}$,
that is, we adjust the domain of the function.
Clearly, $\img(\fnres{f}{A}) \subseteq \img(f)$.

When we write $f \df (\hdots)$,
where for the $\hdots$ (three dots), we give a list of $n$ objects for some $n \in \NN$,
then we define a function $f$ with domain $\setn{n}$,
such that $f_i$ refers to the $i$th object in the list, for each $i \in \setn{n}$.
For example, if we define ${f \df (\set{7,4}, 8, 2)}$, then $f \in \fnset{\setn{3}}{\NN \cup \powset{\NN}}$
and $f_1 = \set{7,4}$, $f_2 = 8$, and $f_3 = 2$.
As another example, consider $g \df ((7,4), 8, 2)$.
Now $g_1 \in \fnset{\setn{2}}{\NN}$ and so ${g \in \fnset{\setn{3}}{\NN \cup \fnset{\setn{2}}{\NN}}}$.

When we write \enquote{let $\set{f_x}_{x \in X} \subseteq Y$},
then this introcudes a function $f \in \fnset{X}{Y}$.
An \term{indexed set} in~$Y$ is an \emphasis{injective} function $f$ with $\img(f) \subseteq Y$.
We allow us to introduce indexed sets like \enquote{let $S = \set{f_x}_{x \in X} \subseteq Y$ be an indexed set},
by which we mean that $f \in \fnset{X}{Y}$ is injective,
and we define $S \df \set{f_x}_{x \in X} = \img(f)$.
We also write \enquote{let $\set{f_x}_{x \in X} \subseteq Y$ be an indexed set},
in order to just introduce an injective function $f \in \fnset{X}{Y}$ without giving a name to its image.
By \enquote{the indexed set}, we may refer to $f$ as well as to its image.
When a finite set $S$ is already introduced, and then we say \enquote{write $S = \seti{f_i}{n}$ as an indexed set},
we mean that an injective function $f \in \fnset{\setn{n}}{S}$ is defined for some $n \in \NN$
such that the image of $f$ is~$S$.
This implies that $n = \card{S}$.
When we say that \enquote{$\set{f_x}_{x \in X}$ is an indexed set},
where $f$ is an already defined function with $\dom(f) = X$,
we express that $f$ is injective.
Simply using the phrase from the beginning of this paragraph,
namely \enquote{let $\set{f_x}_{x \in X} \subseteq Y$},
does \emphasis{not} imply that $f$ is injective.

Let $f \in \fnset{X}{Y}$ be any function and $Y' \subseteq Y$.
We say that $f$ is \term{surjective} on~$Y'$ if $Y' \subseteq \img(f)$.
We say that $f$ is \term{bijective} between $X$ and $Y'$
if $f$ is injective and $\img(f) = Y'$.

Let $f \in \fnset{X}{Y}$ be injective,
that is, $f$ is bijective between $X$ and $\img(f)$.
Then there is a function $g \in \fnset{\img(f)}{X}$ such that $f(g(x)) = x$ for all $x \in \img(f)$
and $g(f(x)) = x$ for all $x \in X$.
We call this function the \term{inverse function} of $f$ and denote it by $f^{-1}$.
In particular, $\dom(f^{-1}) = \img(f)$.

\section*{Cartesian Product}

Let $n \in \NN$ and $\eli{A}{n}$ be sets.
Define:
\begin{IEEEeqnarray*}{c}
  \timeseli{A}{n} \df \prod_{i=1}^n A_i \df \set{ (\eli{a}{n}) \suchthat \forall i \in \setn{n} \holds a_i \in A_i }
\end{IEEEeqnarray*}
This set is the \term{\xfold{n} Cartesian product} of the $\eli{A}{n}$.
It is a set of functions, tuples indeed, formally, $\timeseli{A}{n} \subseteq \fnset{\setn{n}}{\bigcup_{i=1}^n A_i}$.
For $n=2$, this notation is the same as for the set of pairs.
But once we have the notion of a function, pairs in their original form are not required anymore.
Hence we identify each pair with its corresponding tuple of length~$2$.
If $B$ is a set, then define $B^n \df \prod_{i=1}^n B$,
which is the same as $\fnset{\setn{n}}{B}$.
We identify $\prod_{i=1}^1 A_i$ with $A_1$, and we identify $B^1$ with~$B$.

Now, the way we have defined it, for sets $A,B,C$,
it makes a difference in writing $(A \times B) \times C$ or $A \times (B \times C)$ or $A \times B \times C$,
namely:
\begin{IEEEeqnarray*}{l}
  (A \times B) \times C \subseteq \fnset{\setn{2}}{(A \times B) \cup C} \\
  A \times (B \times C) \subseteq \fnset{\setn{2}}{A \cup (B \times C)} \\
  A \times B \times C \subseteq \fnset{\setn{3}}{A \cup B \cup C}
\end{IEEEeqnarray*}
Sometimes, we are given a set $M \subseteq A \times B$
and like to consider the set:
\begin{IEEEeqnarray}{c}
  \label{eq:preface:T}
  T \df \set{(a,b,c) \suchthat (a,b) \in M \land c \in C}
\end{IEEEeqnarray}
This cannot be written as $M \times C$, since the latter would give $\set{(x,c) \suchthat x \in M \land c \in C}$.
We make the convention that by writing
\begin{IEEEeqnarray*}{c}
  T' \df M \times C \subseteq A \times B \times C \comma
\end{IEEEeqnarray*}
we obtain $T' = T$.
That is, adding \enquote{$\subseteq A \times B \times C$} forces the Cartesian product into the desired form.
As another example, consider sets $M$ and $N$ with $M \subseteq N$.
Then $M^2 \times N$ is different from $M \times M \times N$,
but the second one can be obtained by writing $M^2 \times N \subseteq N^3$.
If in doubt whether the notation is clear enough, the desired objects should be written out explicitly,
as we did in~\eqref{eq:preface:T}.
