\chapter{Quadratic Matrices}
\label{cha:cha240}

\section{The Ring of Quadratic Matrices}

\begin{definition}
  Let $R$ be a set and let $+, \cdot \in \fnset{R \times R}{R}$ be two operations on $R$ such that:
  \begin{itemize}
  \item $(R,+)$ is a commutative group, called the \term{additive group} of $R$.
  \item $(R,\cdot)$ is a semigroup, called the \term{multiplicative semigroup} of $R$.
  \item The \term{distributive property} holds,
    that is, $(x+y) \cdot z = x \cdot z + y \cdot z$
    and $z \cdot (x+y) = z \cdot x + z \cdot y$
    for all $x,y,z \in R$.
  \end{itemize}
  Then $(R,+,\cdot)$ is called a \term{ring}.
  If the multiplicative semigroup $(R,\cdot)$ admits a neutral element (which is uniquely determined then),
  the ring $(R,+,\cdot)$ is called a \term{unitary ring}.
  We often denote a unitary ring in the form $(R,0,1,+,\cdot)$,
  meaning that $0$ is the neutral element of the commutative group
  and $1$ is the neutral element of the multiplicative semigroup.\footnote{%
    \enquote{$0$} and \enquote{$1$} are names for those elements
    and need not to coincide with the $0$ or $1$ from the integers, for example;
    see also \autoref{def:cha160:field}.}
  \qed
\end{definition}

Like with any algebraic structure, we often refer to the set $R$ as a ring
when the two operations are clear from context.
Notation from fields is carried over to rings, as far as applicable.
Note that the multiplicative semigroup of a ring does not have to be commutative.
This is the reasong why we considered $(x+y) \cdot z$ and $z \cdot (x+y)$
when stating the distributive property in the preceding definition,
whereas for fields, we could restrict to one of the two expressions.

\begin{remark}
  Let $n \in \NN$ and $\field$ be a field.
  Then $\field^{n \times n}$ is a unitary ring with respect to matrix addition and multiplication
  as defined in \autoref{rem:cha210:mat-add} and \autoref{def:cha220:mat-mul}.
  The neutral element of addition is the \term{zero matrix}, that is,
  the $(n\times n)$~matrix with each entry being~$0$.
  The neutral element of multiplication is the identity matrix $I_n = \diag_n(1)$.
\end{remark}
\begin{proof}
  Follows from \autoref{rem:cha220:diag} and \autoref{rem:cha220:rules}.
\end{proof}

\begin{remark}
  Let $(R,+,\cdot)$ be a ring and $x \in R$.
  Then $0x = 0 = x0$.
\end{remark}
\begin{proof}
  We have $0 x = (0+0) x = 0 x + 0 x$, hence $0=0 x$.
  Likewise, $x 0 = x (0+0) = x 0 + x 0$, hence $0 = x 0$.
\end{proof}

\begin{definition}
  Let $(R,0,1,+,\cdot)$ be a unitary ring.
  We call $x \in R$\,\dots
  \begin{itemize}
  \item \term{left invertible} if there is $y \in R$ such that $yx=1$;
  \item \term{right invertible} if there is $y \in R$ such that $xy=1$;
  \item \term{invertible} or a \term{unit} if it is left invertible and right invertible.
  \end{itemize}
  The element $y$ is called a \term{left inverse}, \term{right inverse}, and \term{(multiplicative) inverse} for $x$, respectively.
  \qed
\end{definition}

\begin{proposition}
  \label{prop:cha240:unit}
  Let $(R,0,1,+,\cdot)$ be a unitary ring, and let $u \in R$.
  Then the following are equivalent:
  \begin{enumerate}
  \item\label{prop:cha240:unit:1} $u$ is a unit.
  \item\label{prop:cha240:unit:2} $u$ has a left inverse and a right inverse.
  \item\label{prop:cha240:unit:3} $u$ has a unique left inverse.
  \item\label{prop:cha240:unit:4} $u$ has a unique right inverse.
  \end{enumerate}
  In particular, in case that $u$ is a unit, its inverse is uniquely determined
  and coincides with the uniquely determined left inverse and right inverse.
\end{proposition}
\begin{proof}
  \impref{prop:cha240:unit:1}{prop:cha240:unit:2} trivial.
  \par
  \impref{prop:cha240:unit:2}{prop:cha240:unit:1}
  Let $a$ be a left inverse and $b$ be a right inverse for $u$.
  We have $a = a 1 = a(ub) = (au)b = 1 b = b$.
  Hence $a=b$ is a left inverse and a right inverse for $u$, hence it is an inverse for $u$.
  \par
  \impref{prop:cha240:unit:1}{prop:cha240:unit:3}
  Let $\tiu$ be an inverse for $u$.
  Let $y,z \in R$ be left inverse elements for $u$.
  Then:
  \begin{IEEEeqnarray*}{c}
    y = y 1
    = y (u\tiu)
    = (y u)\tiu
    = 1 \tiu
    = \tiu = 1 \tiu
    = (zu) \tiu
    = z (u \tiu)
    = z 1 = z
  \end{IEEEeqnarray*}
  Likewise, we can prove \impref*{prop:cha240:unit:1}{prop:cha240:unit:4}.
  \par
  \impref{prop:cha240:unit:3}{prop:cha240:unit:2}
  Let $y$ be the unique left inverse.
  Then:
  \begin{IEEEeqnarray*}{c}
    (1 - uy + y) u = u - uyu + yu = u (1- yu) + 1 = u 0 + 1 = 1
  \end{IEEEeqnarray*}
  So $1 - uy + y$ is a left inverse, so by uniqueness $1 - uy + y = y$,
  hence $1 - uy = 0$, that is, $1 = uy$, so $y$ is also a right inverse for~$u$.
  \par
  Likewise, we can prove \impref*{prop:cha240:unit:4}{prop:cha240:unit:2}.
  \par
  The additional statement about uniqueness follows
  since if $\tiu$ and $\tiu'$ are both inverse elements for $u$,
  they are in particular both left inverse and both right inverse elements.
\end{proof}

\begin{definition}
  Let $(R,0,1,+,\cdot)$ be a unitary ring, and let $u \in R$ be a unit.
  The uniquely determined inverse for $u$ is denoted by $u^{-1}$.
  \qed
\end{definition}

\begin{remark}
  Let $\field$ be a field, and denote by $V \df \fnset{\NN}{\field}$ the vector space of \term{sequences} over $\field$,
  with pointwise addition and pointwise scalar multiplication,
  that is, for all $x,y \in V$ and all $\al \in \field$, we have $x+y = \fn{\NN}{i \mapsto x_i + y_i}$ and $\al x = \fn{\NN}{i \mapsto \al x_i}$.
  Let $\cE$ be the set of all linear maps from $V$ to $V$, such maps are also called \term{endomorphisms} of $V$.
  We endow $\cE$ with the operations or pointwise addition $\oplus$ and composition $\circ$,
  that is, for all $\phi,\psi \in \cE$ we have $\phi \oplus \psi = \fn{V}{x \mapsto \phi(x) + \psi(x)}$
  and $\phi \circ \psi = \fn{V}{x \mapsto \phi(\psi(x))}$.
  \par
  Then $\cE$ is a unitary ring, and there are $\phi \in \cE$ that admit \emphasis{multiple} right inverse elements,
  hence in particular those $\phi$ are \emphasis{right invertible} but \emphasis{not left invertible} and \emphasis{not invertible}.
\end{remark}
\begin{proof}
  Most of the structural statements on $\cE$ follow from easy calculations (see also \autoref{rem:cha220:lin-map-comb}).
  The neutral element of multiplication is the identity function~$\id_V$.
  We show the distributive property.
  To this end, let $\phi,\psi,\rho \in \cE$ and $x \in V$.
  We have:
  \begin{IEEEeqnarray*}{rCl}
    ((\phi \oplus \psi) \circ \rho)(x)
    &=& (\phi \oplus \psi)(\rho(x)) \\
    &=& \phi(\rho(x)) + \psi(\rho(x)) \\
    &=& (\phi \circ \rho)(x) + (\psi \circ \rho)(x) \\
    &=& ((\phi \circ \rho) \oplus (\psi \circ \rho))(x)
  \end{IEEEeqnarray*}
  This shows $(\phi \oplus \psi) \circ \rho = (\phi \circ \rho) \oplus (\psi \circ \rho)$.
  Moreover, using linearity of $\rho$:
  \begin{IEEEeqnarray*}{rCl}
    (\rho \circ (\phi \oplus \psi))(x)
    &=& \rho((\phi \oplus \psi)(x)) \\
    &=& \rho(\phi(x) + \psi(x)) \\
    &=& \rho(\phi(x)) + \rho(\psi(x)) \\
    &=& ((\rho \circ \phi) \oplus (\rho \circ \psi))(x)
  \end{IEEEeqnarray*}
  This shows $\rho \circ (\phi \oplus \psi) = (\rho \circ \phi) \oplus (\rho \circ \phi)$.
  \par
  Now define the \term{left shift}:
  \begin{IEEEeqnarray*}{c}
    \phi \df \fn{V}{x \mapsto \fn{\NN}{i \mapsto x_{i+1}}}
  \end{IEEEeqnarray*}
  That is, $\phi(x_1,x_2,x_3,\hdots) = (x_2,x_3,\hdots)$, for each $x = (x_1, x_2, x_3, \hdots) \in V$.
  It is easy to see that $\phi$ is a linear map from $V$ to $V$.
  For each $a \in \field$ define:
  \begin{IEEEeqnarray*}{c}
    \psi_a \df \fn{V}{x \mapsto \fn{\NN}{i \mapsto \begin{cases} a & i=1 \\ x_{i-1} & i \geq 2 \end{cases}}}
  \end{IEEEeqnarray*}
  That is, $\psi_a(x_1,x_2,x_3,\hdots) = (a,x_1,x_2,x_3,\hdots)$, for each $x = (x_1, x_2, x_3, \hdots) \in V$.
  \par
  For each $a \in \field$ and all $x \in V$, we have $(\phi \circ \psi_a)(x) = \phi(\psi_a(x)) = x$,
  hence $\phi \circ \psi_a = \id_V$.
  So $\psi_a$ is a right inverse for $\phi$.
  Since this works for each $a \in \field$, the claim follows.
\end{proof}

The crucial point in the preceding proof is that $\phi$ is surjective on $V$ but not injective.
This cannot happen for endomorphisms of a \emphasis{finite\-/dimensional} vector space,
which we will now prove.
We will prove it for the vector space $\field^n$ and quadratic matrices representing linear maps,
knowing from \autoref{sec:cha220:lin-to-mat}, \autoref{sec:cha220:lin-to-mat},
and \autoref{thm:cha220:finite-iso} that this covers the general finite\-/dimensional case.

\begin{definition}
  Let $n \in \NN$, let $\field$ be a field, and let $A \in \field^{n \times n}$.
  \begin{enumerate}
  \item We say that $A$ is \term{surjective} if $A$ is surjective on $\field^n$.
  \item We say that $A$ has \term{full rank} if $\rank(A) = n$.\qed
  \end{enumerate}
\end{definition}

\begin{theorem}
  Let $n \in \NN$, let $\field$ be a field, and let $A \in \field^{n \times n}$.
  Then the following are equivalent:
  \begin{enumerate}
  \item $A$ is left invertible.
  \item $A$ is right invertible.
  \item $A$ is injective.
  \item $A$ is surjective.
  \item $A$ has full rank.
  \item $A$ is bijective.
  \item $A$ is invertible.
  \end{enumerate}
\end{theorem}
\begin{proof}
  We prove this without using \autoref{prop:cha240:unit}.
  In the following figure, only the implications marked as (a), (b), and (c) are not clear yet.
  The other statements follow from \autoref{prop:cha22:rank-injectiv-surjektiv}.
  Moreover, if the matrix is invertible, trivially it is left invertible and right invertible
  (the implication not shown in the figure).
  So we are done when we prove (a), (b), and (c).
  \begin{center}
    \begin{tikzpicture}[-{Latex[scale=2.0,length=2,width=3]}]
      \node (i) {left inv.};
      \node (ii) [below = of i] {right inv.};
      \node (iii) [right = of i] {injective};
      \node (iv) [right = of ii] {surjective};
      \node (v) [above right = 0.3 and 1 of iv] {full rank};
      \node (vi) [right = of v] {bijective};
      \node (vii) [right = of vi] {inv.};
      \path (i) edge[double] node [above] {(a)} (iii);
      \path (ii) edge[double] node [above] {(b)} (iv);
      \path (iii) edge[double] (v);
      \path (iv) edge[double] (v);
      \path (v) edge[double] (vi);
      \path (vi) edge[double] node [above] {(c)} (vii);
    \end{tikzpicture}
  \end{center}
  (a) Let $A$ be left invertible, so there is $B \in \field^{n \times n}$ such that $BA=I_n$.
  Let $x,y \in \field^n$ such that $Ax = Ay$.
  It follows $BA x = BA y$, hence $I_n x = I_n y$, thus $x = y$.
  \par
  (b) Let $A$ be right invertible, so there is $B \in \field^{n \times n}$ such that $AB=I_n$.
  Let $y \in \field^n$. Define $x \df By$.
  Then $Ax = ABy = I_n y = y$, so $x$ is mapped to $y$ by $A$.
  \par
  (c) Let $A$ be bijective.
  By \autoref{prop:cha220:basics}, its inverse map is also linear,
  hence can be expressed by a matrix.
  This matrix is the inverse for $A$.
\end{proof}

\section{Computing the Inverse Matrix}

\begin{definition}
  Let $n \in \NN$ and let $\field$ be a field.
  For each $i,j \in \setn{n}$ with $i \neq j$
  and each $\lam \in \fieldnz$, define the following matrices in $\field^{n \times n}$:
  \begin{itemize}
  \item The matrix $E(i,j)$ is defined by the rule:
    \begin{IEEEeqnarray*}{c}
      \forall s,t \in \setn{n} \holds E(i,j)_{s,t} =
      \begin{cases}
        1 & s=i \land j=t \\
        0 & \text{otherwise}
      \end{cases}
    \end{IEEEeqnarray*}
    That is, $E(i,j)$ is the matrix with a $1$ in its $(i,j)$th entry,
    while all other entries are~$0$.
  \item $P(i,j) \df I_n - E(i,i) - E(j,j) + E(i,j) + E(j,i)$,
    that is, $P(i,j)$ is obtained from $I_n$ be swapping rows $i$ and $j$.
  \item $S(i, \lam) \df I_n + (\lam - 1) E(i,j)$,
    that is, $S(i,\lam)$ is a diagonal matrix with the $(i,i)$th entry being~$\lam$
    and the other diagonal entries being~$1$.
  \item $Q(i,j,\lam) \df I_n + \lam E(i,j)$,
    that is, $Q(i,j,\lam)$ is obtained from $I_n$ by changing the $(i,j)$th entry from~$0$ to~$\lam$;
    recall that $i \neq j$.
  \end{itemize}
  The last three types of matrices, namely $P(i,j)$, $S(i,\lam)$, and $Q(i,j,\lam)$,
  are called \term{elementary matrices}.
  \qed
\end{definition}

\begin{proposition}
  \label{prop:cha240:row-op}
  Let $n \in \NN$ and let $\field$ be a field.
  Let $i,j \in \setn{n}$ with $i \neq j$, and let $\lam \in \fieldnz$.
  Let $A \in \field^{n \times n}$.
  Then:
  \begin{enumerate}
  \item $P(i,j) \cdot A$ can be obtained from $A$ by swapping rows $i$ and $j$.
  \item $S(i,\lam) \cdot A$ can be obtained from $A$ by scaling row $i$ with $\lam$.
  \item $Q(i,j,\lam) \cdot A$ can be obtained from $A$ by adding $\lam$ times row $j$ to row $i$.
  \end{enumerate}
\end{proposition}
\begin{proof}
  By definition of matrix multiplication,
  for each $s \in \setn{n}$, row~$s$ of the elementary matrix
  is responsible for creating row~$s$ of the product of the elementary matrix with the matrix $A$.
  Hence in $P(i,j) \cdot A$, whenever $s \not\in \set{i,j}$,
  then row~$s$ of the product is the same as in $A$;
  for $S(i,\lam) \cdot A$ and $Q(i,j,\lam) \cdot A$, this is already true when $s \neq i$.
  \begin{enumerate}
  \item We consider row $i$ in $B \df P(i,j) \cdot A$.
    Row $i$ of $P(i,j)$, which is responsible for row $i$ in $B$,
    has a $1$ in entry~$j$ and zeros otherwise.
    Hence it will produce row~$j$ of $A$, as claimed.
    The same argument shows that row~$j$ in $B$ is row~$i$ of $A$.
  \item We consider row~$i$ in $B \df S(i,\lam) \cdot A$.
    Row~$i$ of $P(i,j)$, which is responsible for row~$i$ in $B$,
    has $\lam$ in entry~$i$ and zeros otherwise.
    Hence it will produce $\lam$~times row~$i$ of~$A$, as claimed.
  \item We consider row $i$ in $B \df Q(i,j,\lam) \cdot A$.
    Row $i$ of $Q(i,j,\lam)$, which is responsible for row $i$ in $B$,
    has $\lam$ in entry~$j$, a~$1$ in entry~$i$, and zeros otherwise.
    Hence it procudes $\lam$~times row~$j$ of~$A$ plus row~$i$ of~$A$, as claimed.
    \qedhere
  \end{enumerate}
\end{proof}

\begin{proposition}
  Elementary matrices are invertible, and their inverse matrices are again elementary matrices.
  In particular, let $n \in \NN$, and let $\field$ be a field.
  For each $i,j \in \setn{n}$ with $i \neq j$ and each $\lam \in \fieldnz$, we have:
  \begin{itemize}
  \item $(P(i,j))^{-1} = P(i,j)$
  \item $(S(i,\lam))^{-1} = S(i,\lam^{-1})$
  \item $(Q(i,j,\lam))^{-1} = Q(i,j,-\lam)$
  \end{itemize}
\end{proposition}
\begin{proof}
  Follows from \autoref{prop:cha240:row-op}:
  swapping two rows is reversed by swapping them again,
  scaling a row with $\lam$ is reversed by scaling it with $\lam^{-1}$,
  and adding~$\lam$ times row~$j$ to row~$i$
  is reversed by adding $-\lam$~times row~$j$ to row~$i$.
\end{proof}

\begin{proposition}
  \label{prop:cha240:col-swap}
  Let $n \in \NN$, let $\field$ be a field, and let $A \in \field^{n \times n}$.
  We apply \autoref{alg:cha230:crref} to $A$.
  Then, if we need one or more column swaps,
  we may conclude that $A$ does \emphasis{not} have full rank.
\end{proposition}
\begin{proof}
  Let $j$ be the index (see description of the algorithm) where the first column swap is required.
  Then starting from its $j$th row, this column has all zeros.
  This will not change during the future course of the algorithm since
  only multiples of rows with an index at least $j$ will be added to other rows
  from this point forward.
  Hence it is impossible for the final matrix to be $I_n$.
  Since $I_n$ is the outcome if and only if $A$ has full rank, the claim follows.
\end{proof}

This gives us an algorithm to decide whether a matrix is invertible
and to compute its inverse if so:
\begin{itemize}
\item Input: $A \in \field^{n \times n}$ for a field $\field$ and some $n \in \NN$.
\item Apply \autoref{alg:cha230:crref} to $A$.
  While we do so, we apply the \emphasis{same elementary row operations} that we use on $A$ also to the identity matrix $I_n$;
  denote the result of this by $B \in \field^{n \times n}$.
\item If a column swap is required, report that $A$ is \emphasis{not invertible}.
\item Otherwise return $B$ as the inverse.
\end{itemize}

\begin{proposition}
  The above algorithm is correct.
\end{proposition}
\begin{proof}
  By \autoref{prop:cha240:col-swap},
  the matrix $A$ is indeed not invertible once a column swap is required.
  Otherwise, we have elementary matrices $\eli{T}{r}$ for some $r \in \NN$ such that:
  \begin{IEEEeqnarray*}{c+c}
    T_1 \cdot \hdots \cdot T_r \cdot A = I_n
    &
    T_1 \cdot \hdots \cdot T_r \cdot I_n = B
  \end{IEEEeqnarray*}
  It follows $A = T_r^{-1} \cdot \hdots \cdot T_1^{-1}$ and $B = T_1 \cdot \hdots \cdot T_r$, and thus:
  \begin{IEEEeqnarray*}{rCl}
    A B &=& T_r^{-1} \cdot \hdots \cdot T_1^{-1} \cdot T_1 \cdot \hdots \cdot T_r \\
    &=& T_r^{-1} \cdot \hdots \cdot T_2^{-1} \cdot T_2 \cdot \hdots \cdot T_r \\
    &=& \hdots =  T_r^{-1} \cdot T_r = I_n
  \end{IEEEeqnarray*}
  Hence $B$ is a right inverse for $A$ and hence the inverse for $A$.
\end{proof}

\begin{example}
  We want to know if
  \begin{equation*}
    A \df
    \begin{gmatrix}[b]
      13 & 11 & 17 \\
      12 & 27 & 9 \\
      2 & 6 & 18
    \end{gmatrix}
  \end{equation*}
  is invertible and if so, compute its inverse.
  We do elementary row operations,
  on the left\-/hand side starting with $A$ and on the right\-/hand side starting with $I_3$.
  \begin{IEEEeqnarray*}{l+l}
    \begin{gmatrix}[b]
      13 & 11 & 17 \\
      12 & 27 & 9 \\
      2 & 6 & 18
      \rowops
      \mult{2}{\cdot \tfrac{1}{2}}
      \swap{0}{2}
    \end{gmatrix}&
    \begin{gmatrix}[b]
      1 & 0 & 0 \\
      0 & 1 & 0 \\
      0 & 0 & 1
      \rowops
      \mult{2}{\cdot \tfrac{1}{2}}
      \swap{0}{2}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 3 & 9 \\
      12 & 27 & 9 \\
      13 & 11 & 17
      \rowops
      \add[-12]{0}{1}
      \add[-13]{0}{2}
    \end{gmatrix}&
    \begin{gmatrix}[b]
      0 & 0 & \tfrac{1}{2} \\
      0 & 1 & 0 \\
      1 & 0 & 0 
      \rowops
      \add[-12]{0}{1}
      \add[-13]{0}{2}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 3 & 9 \\
      0 & -9 & -99 \\
      0 & -28 & -100
      \rowops
      \mult{1}{\cdot -\tfrac{1}{9}}
    \end{gmatrix}&
    \begin{gmatrix}[b]
      0 & 0 & \tfrac{1}{2} \\
      0 & 1 & -6 \\
      1 & 0 & -\tfrac{13}{2}
      \rowops
      \mult{1}{\cdot -\tfrac{1}{9}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 3 & 9 \\
      0 & 1 & 11 \\
      0 & -28 & -100
      \rowops
      \add[-3]{1}{0}
      \add[28]{1}{2}
    \end{gmatrix}&
    \begin{gmatrix}[b]
      0 & 0 & \tfrac{1}{2} \\
      0 & -\tfrac{1}{9} & \tfrac{2}{3} \\
      1 & 0 & -\tfrac{13}{2}
      \rowops
      \add[-3]{1}{0}
      \add[28]{1}{2}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 & -24 \\
      0 & 1 & 11 \\
      0 & 0 & 208
      \rowops
      \mult{2}{\cdot\tfrac{1}{208}}
    \end{gmatrix}&
    \begin{gmatrix}[b]
      0 & \tfrac{1}{3} & -\tfrac{3}{2} \\
      0 & -\tfrac{1}{9} & \tfrac{2}{3} \\
      1 & -\tfrac{28}{9} & \tfrac{73}{6}
      \rowops
      \mult{2}{\cdot\tfrac{1}{208}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 & -24 \\
      0 & 1 & 11 \\
      0 & 0 & 1
      \rowops
      \add[-11]{2}{1}
      \add[24]{2}{0}
    \end{gmatrix}&
    \begin{gmatrix}[b]
      0 & \tfrac{1}{3} & -\tfrac{3}{2} \\
      0 & -\tfrac{1}{9} & \tfrac{2}{3} \\
      \tfrac{1}{208} & -\tfrac{7}{468} & \tfrac{73}{1248}
      \rowops
      \add[-11]{2}{1}
      \add[24]{2}{0}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 & 0 \\
      0 & 1 & 0 \\
      0 & 0 & 1
      \rowops
    \end{gmatrix}&
    \begin{gmatrix}[b]
      \tfrac{3}{26} & -\tfrac{1}{39} & -\tfrac{5}{52} \\
      -\tfrac{11}{208} & \tfrac{25}{468} & \tfrac{87}{3744} \\
      \tfrac{1}{208} & -\tfrac{7}{468} & \tfrac{73}{1248}
      \rowops
    \end{gmatrix}
  \end{IEEEeqnarray*}
  Hence $A$ is invertible, and we have:
  \begin{IEEEeqnarray*}{c}
    A^{-1} = 
    \begin{bmatrix}
      \tfrac{3}{26} & -\tfrac{1}{39} & -\tfrac{5}{52} \\
      -\tfrac{11}{208} & \tfrac{25}{468} & \tfrac{87}{3744} \\
      \tfrac{1}{208} & -\tfrac{7}{468} & \tfrac{73}{1248}
    \end{bmatrix}
  \end{IEEEeqnarray*}
  For demonstration purposes, we compute the first row of the product between $A$ and that matrix:
  \begin{smallerenv}
    \begin{IEEEeqnarray*}{l}
      \begin{bmatrix}
        13 & 11 & 17 \\
        12 & 27 & 9 \\
        2 & 6 & 18
      \end{bmatrix}
      \cdot
      \begin{bmatrix}
        \tfrac{3}{26} & -\tfrac{1}{39} & -\tfrac{5}{52} \\
        -\tfrac{11}{208} & \tfrac{25}{468} & \tfrac{87}{3744} \\
        \tfrac{1}{208} & -\tfrac{7}{468} & \tfrac{73}{1248}
      \end{bmatrix}\\
      =
      \begin{bmatrix}
        13\cdot\tfrac{3}{26}-11\cdot\tfrac{11}{208}+17\cdot\tfrac{1}{208}
        &-13\cdot\tfrac{1}{39}+11\cdot\tfrac{25}{468}-17\cdot\tfrac{7}{468}
        &-13\cdot\tfrac{5}{52}+11\cdot\tfrac{87}{3744}+17\cdot\tfrac{73}{1248}\\
        \hdots & \hdots & \hdots \\
        \hdots & \hdots & \hdots
      \end{bmatrix}\\
      =
      \begin{bmatrix}
        \tfrac{312-121+17}{208}&\tfrac{-156+275-119}{468}&\tfrac{-4680+957+3723}{3744}\\
        \hdots & \hdots & \hdots \\
        \hdots & \hdots & \hdots
      \end{bmatrix}\\
      =
      \begin{bmatrix}
        1 & 0 & 0\\
        \hdots & \hdots & \hdots \\
        \hdots & \hdots & \hdots
      \end{bmatrix}
    \end{IEEEeqnarray*}
  \end{smallerenv}
  So far, the result agrees with $I_3$, as required.\qed
\end{example}

Recall \autoref{ex:cha220:xchange}.
We will now learn the systematic way promised there for computing the matrix of a linear map.

\begin{proposition}
  Let $\field$ be a field and $\phi \in \fnset{\field^n}{\field^d}$ be linear for some ${d,n \in \NN}$.
  Let $\seti{b_i}{n}$ be an ordered basis of~$\field^n$.
  Define $A \df \RowVec{\phi(b_1) , \hdots , \phi(b_n)} \in \field^{d \times n}$
  and $B \df \RowVec{b_1 , \hdots , b_n} \in \field^{n \times n}$.
  Then $B$ is invertible, and $C \df AB^{-1}$ is the matrix for $\phi$,
  that is, $\phi = \phi_C$.
\end{proposition}
\begin{proof}
  Since $\seti{b_i}{n}$ is linearly independent, we have $\rank(B)=n$, hence $B$ is invertible.
  The matrix $A$ has $n$ columns and $B^{-1}$ has $n$ rows, so $AB^{-1}$ is defined.
  For each $j \in \setn{n}$, the vector $b_j$ is the $j$th column of $B$, and so:
  \begin{equation*}
    B^{-1} b_j = (B^{-1} B)_{\ast,j} = (I_n)_{\ast,j} = e_j
  \end{equation*}
  If follows $AB^{-1}b_j = Ae_j = \phi(b_j)$.
  Hence $AB^{-1}$ and $\phi$ coincide on a basis (namely on $\seti{b_i}{n}$),
  hence these two mappings coincide.
\end{proof}

\begin{example}
  We repeat \autoref{ex:cha220:xchange} with this method.
  Recall that we have:
  \begin{IEEEeqnarray*}{c+c}
    \phi\parens{\colvec{5\\5}} = \colvec{2\\2}
    &
    \phi\parens{\colvec{3\\0}} = \colvec{0\\1}
  \end{IEEEeqnarray*}
  The matrix $B$ to invert is thus:
  \begin{IEEEeqnarray*}{c}
    B =
    \begin{bmatrix}
      5 & 3 \\ 5 & 0
    \end{bmatrix}
  \end{IEEEeqnarray*}
  We compute its inverse:
  \begin{IEEEeqnarray*}{l+l}
    \begin{gmatrix}[b]
      5 & 3 \\ 5 & 0
      \rowops
      \mult{1}{\cdot \tfrac{1}{5}}
      \swap{0}{1}
    \end{gmatrix}
    &
    \begin{gmatrix}[b]
      1 & 0 \\ 0 & 1
      \rowops
      \mult{1}{\cdot \tfrac{1}{5}}
      \swap{0}{1}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 \\
      5 & 3
      \rowops
      \add[-5]{0}{1}
    \end{gmatrix}
    &
    \begin{gmatrix}[b]
      0 & \tfrac{1}{5} \\
      1 & 0
      \rowops
      \add[-5]{0}{1}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 \\
      0 & 3
      \rowops
      \mult{1}{\cdot \tfrac{1}{3}}
    \end{gmatrix}
    &
    \begin{gmatrix}[b]
      0 & \tfrac{1}{5} \\
      1 & -1
      \rowops
      \mult{1}{\cdot \tfrac{1}{3}}
    \end{gmatrix}\\
    \begin{gmatrix}[b]
      1 & 0 \\
      0 & 1
    \end{gmatrix}
    &
    \begin{gmatrix}[b]
      0 & \tfrac{1}{5} \\
      \tfrac{1}{3} & -\tfrac{1}{3}
    \end{gmatrix}
  \end{IEEEeqnarray*}
  Then we multiply the matrix containing the images of the basis vectors as columns
  with the matrix~$B^{-1}$:
  \begin{IEEEeqnarray*}{c}
    \begin{bmatrix}
      2 & 0\\
      2 & 1
    \end{bmatrix}
    \cdot
    \begin{bmatrix}
      0 & \tfrac{1}{5} \\
      \tfrac{1}{3} & -\tfrac{1}{3}
    \end{bmatrix}
    =
    \begin{bmatrix}
      2 \cdot 0 + 0 \cdot \tfrac{1}{3} & 2 \cdot \tfrac{1}{5} + 0 \cdot \parens{-\tfrac{1}{3}} \\
      2 \cdot 0 + 1 \cdot \tfrac{1}{3} & 2 \cdot \tfrac{1}{5} + 1 \cdot \parens{-\tfrac{1}{3}}
    \end{bmatrix}
    =
    \begin{bmatrix}
      0 & \tfrac{2}{5} \\
      \tfrac{1}{3} & \tfrac{1}{15}
    \end{bmatrix}
  \end{IEEEeqnarray*}
  This is exactly the matrix that we got the first time, see \autoref{ex:cha220:xchange}.
  \qed
\end{example}

\section{Determinant}

%%% 
%%% TODO
%%% 

\section{Eigenvalues}

%%% 
%%% TODO
%%% 
